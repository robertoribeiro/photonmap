 #!/bin/bash


sort -b -k2 -k3 -n logC.txt > tmp; mv tmp logC.txt
sort -b -k2 -k3 -n logG.txt > tmp; mv tmp logG.txt


paste -d' ' logG.txt logC.txt | awk '{printf("%.10f %.10f %.10f\n", $2 - $6, $3 - $7, $4 - $8)}' > diff_result.txt
