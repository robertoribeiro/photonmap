/*
 * CUDA_Worker.cpp
 *
 *  Created on: Oct 31, 2012
 *      Author: rr
 */

#include "CPU_Worker.h"
#include "omp.h"

CPU_Worker::~CPU_Worker() {

}

void CPU_Worker::Render(bool buildHitPoints) {

#ifdef USE_AFFINITY

#ifdef USE_SOCKET_AFFINITY
	hwloc_set_thread_cpubind(cfg->topology, pthread_self(), numa->cpuset, 0);
#endif

#ifndef __DEBUG
	omp_set_num_threads(NUM_THREADS);

#pragma omp parallel for schedule(static)
	for (uint i = 0; i < NUM_THREADS; i++) {

#ifdef USE_SOCKET_AFFINITY
		hwloc_set_cpubind(cfg->topology, numa->cpuset, HWLOC_CPUBIND_THREAD);
#endif

#ifdef USE_SOCKET_CORE_AFFINITY
		uint core_logical_id = (i % 6) + (deviceID - CPU0)*6;
		hwloc_obj_t core = hwloc_get_obj_by_type(cfg->topology, HWLOC_OBJ_CORE,core_logical_id);
		hwloc_set_cpubind(cfg->topology, core->cpuset, HWLOC_CPUBIND_THREAD);

		//		printf("Thread %d %d bind to core %d\n", omp_get_thread_num(), deviceID,
		//				core_logical_id);

#endif

	}
#endif

#endif

	__p.reg("Process Iterations");

	engine->ProcessIterations(this, buildHitPoints);

	__p.stp("Process Iterations");

}

void CPU_Worker::InitHitPoints() {

//	HPsIterationRadianceFlux = new HitPointRadianceFlux[cfg->hitPointTotal];
//	memset(HPsIterationRadianceFlux, 0,
//			sizeof(HitPointRadianceFlux) * cfg->hitPointTotal);

//if (cfg->GetEngineType() != PPMPA)
	HPsPositionInfo = new HitPoint[cfg->GetHitPointTotal()];
	memset(HPsPositionInfo, 0, sizeof(HitPoint) * cfg->GetHitPointTotal());
	//else
	//	HPsPositionInfo = engine->GetHitPoints();

}

void CPU_Worker::AdvanceEyePaths(RayBuffer *rayBuffer, EyePath* todoEyePaths,
		uint* eyePathIndexes) {

#ifndef __DEBUG
	omp_set_num_threads(NUM_THREADS);
#pragma omp parallel for schedule(guided)
#endif
	for (uint i = 0; i < rayBuffer->GetRayCount(); i++) {

		//Bind(omp_get_thread_num());

		EyePath *eyePath = &todoEyePaths[eyePathIndexes[i]];

		const RayHit *rayHit = &rayBuffer->GetHitBuffer()[i];

		if (rayHit->Miss()) {
			// Add an hit point

			HitPoint* hp = GetHitPoint(eyePath->sampleIndex);

			hp->type = CONSTANT_COLOR;
			hp->scrX = eyePath->scrX;
			hp->scrY = eyePath->scrY;

			if (ss->infiniteLight || ss->sunLight || ss->skyLight) {

				if (ss->infiniteLight)
					ss->InfiniteLight_Le(&hp->throughput, &eyePath->ray.d,
							ss->infiniteLight, ss->infiniteLightMap);
				if (ss->sunLight)
					ss->SunLight_Le(&hp->throughput, &eyePath->ray.d,
							ss->sunLight);
				if (ss->skyLight)
					ss->SkyLight_Le(&hp->throughput, &eyePath->ray.d,
							ss->skyLight);

				hp->throughput *= eyePath->throughput;
			} else
				hp->throughput = Spectrum();
			eyePath->done = true;

		} else {

			// Something was hit
			Point hitPoint;
			Spectrum surfaceColor;
			Normal N, shadeN;

			if (engine->GetHitPointInformation(ss, &eyePath->ray, rayHit,
					hitPoint, surfaceColor, N, shadeN))
				continue;

			// Get the material
			const unsigned int currentTriangleIndex = rayHit->index;
			const unsigned int currentMeshIndex =
					ss->meshIDs[currentTriangleIndex];

			const uint materialIndex = ss->meshMats[currentMeshIndex];

			POINTERFREESCENE::Material *hitPointMat =
					&ss->materials[materialIndex];

			uint matType = hitPointMat->type;

			if (matType == MAT_AREALIGHT) {

				// Add an hit point
				HitPoint* hp = GetHitPoint(eyePath->sampleIndex);

				hp->type = CONSTANT_COLOR;
				hp->scrX = eyePath->scrX;
				hp->scrY = eyePath->scrY;

				Vector md = -eyePath->ray.d;
				ss->AreaLight_Le(&hitPointMat->param.areaLight, &md, &N,
						&hp->throughput);
				hp->throughput *= eyePath->throughput;

				// Free the eye path
				eyePath->done = true;

			} else {

				Vector wo = -eyePath->ray.d;
				float materialPdf;

				Vector wi;
				bool specularMaterial = true;
				float u0 = getFloatRNG(seedBuffer[eyePath->sampleIndex]);
				float u1 = getFloatRNG(seedBuffer[eyePath->sampleIndex]);
				float u2 = getFloatRNG(seedBuffer[eyePath->sampleIndex]);
				Spectrum f;

				switch (matType) {

				case MAT_MATTE:
					ss->Matte_Sample_f(&hitPointMat->param.matte, &wo, &wi,
							&materialPdf, &f, &shadeN, u0, u1,
							&specularMaterial);
					f *= surfaceColor;
					break;

				case MAT_MIRROR:
					ss->Mirror_Sample_f(&hitPointMat->param.mirror, &wo, &wi,
							&materialPdf, &f, &shadeN, &specularMaterial);
					f *= surfaceColor;
					break;

				case MAT_GLASS:
					ss->Glass_Sample_f(&hitPointMat->param.glass, &wo, &wi,
							&materialPdf, &f, &N, &shadeN, u0,
							&specularMaterial);
					f *= surfaceColor;

					break;

				case MAT_MATTEMIRROR:
					ss->MatteMirror_Sample_f(&hitPointMat->param.matteMirror,
							&wo, &wi, &materialPdf, &f, &shadeN, u0, u1, u2,
							&specularMaterial);
					f *= surfaceColor;

					break;

				case MAT_METAL:
					ss->Metal_Sample_f(&hitPointMat->param.metal, &wo, &wi,
							&materialPdf, &f, &shadeN, u0, u1,
							&specularMaterial);
					f *= surfaceColor;

					break;

				case MAT_MATTEMETAL:
					ss->MatteMetal_Sample_f(&hitPointMat->param.matteMetal, &wo,
							&wi, &materialPdf, &f, &shadeN, u0, u1, u2,
							&specularMaterial);
					f *= surfaceColor;

					break;

				case MAT_ALLOY:
					ss->Alloy_Sample_f(&hitPointMat->param.alloy, &wo, &wi,
							&materialPdf, &f, &shadeN, u0, u1, u2,
							&specularMaterial);
					f *= surfaceColor;

					break;

				case MAT_ARCHGLASS:
					ss->ArchGlass_Sample_f(&hitPointMat->param.archGlass, &wo,
							&wi, &materialPdf, &f, &N, &shadeN, u0,
							&specularMaterial);
					f *= surfaceColor;

					break;

				case MAT_NULL:
					wi = eyePath->ray.d;
					specularMaterial = 1;
					materialPdf = 1.f;

					// I have also to restore the original throughput
					//throughput = prevThroughput;
					break;

				default:
					// Huston, we have a problem...
					specularMaterial = 1;
					materialPdf = 0.f;
					break;

				}
				if ((materialPdf <= 0.f) || f.Black()) {

					// Add an hit point
					HitPoint* hp = GetHitPoint(eyePath->sampleIndex);
					hp->type = CONSTANT_COLOR;
					hp->scrX = eyePath->scrX;
					hp->scrY = eyePath->scrY;
					hp->throughput = Spectrum();

					eyePath->done = true;
				} else if (specularMaterial || (!hitPointMat->difuse)) {

					eyePath->throughput *= f / materialPdf;
					eyePath->ray = Ray(hitPoint, wi);
				} else {
					// Add an hit point
					HitPoint* hp = GetHitPoint(eyePath->sampleIndex);
					hp->type = SURFACE;
					hp->scrX = eyePath->scrX;
					hp->scrY = eyePath->scrY;

					hp->materialSS = materialIndex;

					hp->throughput = eyePath->throughput * surfaceColor;
					hp->position = hitPoint;
					hp->wo = -eyePath->ray.d;
					hp->normal = shadeN;

					// Free the eye path
					eyePath->done = true;

				}

			}

		}
	}

}

void CPU_Worker::ProcessEyePaths() {

	double start = WallClockTime();

	const unsigned int width = cfg->width;
	const unsigned int height = cfg->height;
	const unsigned int superSampling = cfg->superSampling;

	const unsigned int hitPointTotal = cfg->GetHitPointTotal();

	EyePath* todoEyePaths = new EyePath[hitPointTotal];

	unsigned int hitPointsIndex = 0;
	const float invSuperSampling = 1.f / superSampling;

	for (unsigned int y = 0; y < height; ++y) {
		for (unsigned int x = 0; x < width; ++x) {

			for (unsigned int sy = 0; sy < superSampling; ++sy) {
				for (unsigned int sx = 0; sx < superSampling; ++sx) {

					EyePath *eyePath = &todoEyePaths[hitPointsIndex];

					eyePath->scrX = x
							+ (sx + getFloatRNG(seedBuffer[hitPointsIndex]))
									* invSuperSampling - 0.5f;

					eyePath->scrY = y
							+ (sy + getFloatRNG(seedBuffer[hitPointsIndex]))
									* invSuperSampling - 0.5f;

					float u0 = getFloatRNG(seedBuffer[hitPointsIndex]);
					float u1 = getFloatRNG(seedBuffer[hitPointsIndex]);
					float u2 = getFloatRNG(seedBuffer[hitPointsIndex]);

					ss->GenerateRay(eyePath->scrX, eyePath->scrY, width, height,
							&eyePath->ray, u0, u1, u2, &ss->camera);

					eyePath->depth = 0;
					eyePath->throughput = Spectrum(1.f, 1.f, 1.f);

					eyePath->done = false;
					eyePath->splat = false;
					eyePath->sampleIndex = hitPointsIndex;

					HitPoint* hp = GetHitPoint(hitPointsIndex);

					hp->id = hitPointsIndex++;

				}
			}
		}
	}

	uint todoEyePathCount = cfg->GetHitPointTotal();
	uint chunk_counter = 0;
	unsigned long long r = 0;
	uint* eyePathIndexes = new uint[getRaybufferSize()];

	while (todoEyePathCount > 0) {

		//transversing in chunks
		uint start = (chunk_counter / getRaybufferSize()) * getRaybufferSize();

		uint end;
		if (cfg->GetHitPointTotal() - start < getRaybufferSize())
			end = cfg->GetHitPointTotal();
		else
			end = start + getRaybufferSize();

		for (uint i = start; i < end; i++) {

			EyePath *eyePath = &todoEyePaths[i];

			// Check if we reached the max path depth
			if (!eyePath->done && eyePath->depth > MAX_EYE_PATH_DEPTH) {

				// Add an hit point
				HitPoint* hp = GetHitPoint(eyePath->sampleIndex);

				hp->type = CONSTANT_COLOR;
				hp->scrX = eyePath->scrX;
				hp->scrY = eyePath->scrY;
				hp->throughput = Spectrum();

				eyePath->done = true;

			} else if (!eyePath->done) {
				eyePath->depth++;

				uint p = RaybufferAddRay(eyePath->ray);

				eyePathIndexes[p] = i;
			}

			if (eyePath->done && !eyePath->splat) {
				--todoEyePathCount;
				chunk_counter++;
				eyePath->splat = true;
			}
		}

		if (getRayBufferRayCount() > 0) {

			IntersectRayBuffer();

			r += rayBuffer->GetRayCount();

			AdvanceEyePaths(&todoEyePaths[0], eyePathIndexes);

			resetRayBuffer();
		}
	}

	profiler->addRayTracingTime(WallClockTime() - start);
	profiler->addEyesamplesRaysTraced(r);
	profiler->addEyesamplesTraced(cfg->GetHitPointTotal());

	delete[] eyePathIndexes;
	delete[] todoEyePaths;

//	for (int i = 0; i < cfg->GetHitPointTotal(); i++) {
//			printf("%d: %.10f %.10f %.10f\n", i, GetHitPoint(i)->position.x,
//					GetHitPoint(i)->position.y,GetHitPoint(i)->position.z);
//		}

}

void CPU_Worker::IntersectRay(const Ray *ray, RayHit *rayHit) {

	ss->dataSet->Intersect(ray, rayHit);

}

void CPU_Worker::Intersect(RayBuffer *rayBuffer) {

	// Trace rays
	const Ray *rb = rayBuffer->GetRayBuffer();
	RayHit *hb = rayBuffer->GetHitBuffer();

#ifndef __DEBUG
	omp_set_num_threads(NUM_THREADS);
#pragma omp parallel for schedule(guided)
#endif
	for (unsigned int i = 0; i < rayBuffer->GetRayCount(); ++i) {
		//Bind(omp_get_thread_num());
		hb[i].SetMiss();
		IntersectRay(&rb[i], &hb[i]);
	}

}

void CPU_Worker::advancePhotons(uint *donePhotonCount, uint *rayCount,
		uint *photonHitCount, const u_int64_t photonTarget) {

	Ray currentRay = Ray();
	PhotonPath currentPhotonPath = PhotonPath();
	RayHit rayHit;

	*rayCount = 0;

	int photonID = AtomicInc(donePhotonCount);

	engine->InitPhotonPath(engine->ss, &currentPhotonPath, &currentRay,
			seedBuffer[photonID]);

	while (photonID != -1) {

		rayHit.SetMiss();
		IntersectRay(&currentRay, &rayHit);

		(*rayCount)++;

//		if (photonID == 3) {
////			printf("ray %.2f %.2f %.2f %.2f %.2f %.2f", currentRay.o.x, currentRay.o.y, currentRay.o.z,
////					currentRay.d.x, currentRay.d.x, currentRay.d.x);
////
////			printf("  hit %.2f %.2f %u\n", rayHit.b1, rayHit.b2,rayHit.index);
//
//		}

		if (rayHit.Miss()) {
			currentPhotonPath.done = true;
		} else { // Something was hit

			Point hitPoint;
			Spectrum surfaceColor;
			Normal N, shadeN;

			if (engine->GetHitPointInformation(engine->ss, &currentRay, &rayHit,
					hitPoint, surfaceColor, N, shadeN)) {
				assert(false);
			}

			//surfaceColor = Spectrum();

			const unsigned int currentTriangleIndex = rayHit.index;
			const unsigned int currentMeshIndex =
					engine->ss->meshIDs[currentTriangleIndex];

			POINTERFREESCENE::Material *hitPointMat =
					&engine->ss->materials[engine->ss->meshMats[currentMeshIndex]];

			uint matType = hitPointMat->type;
			if (matType == MAT_AREALIGHT) {

				currentPhotonPath.done = true;
			} else {

				float fPdf;
				Vector wi;
				Vector wo = -currentRay.d;
				bool specularBounce = true;

				float u0 = getFloatRNG(seedBuffer[photonID]);
				float u1 = getFloatRNG(seedBuffer[photonID]);
				float u2 = getFloatRNG(seedBuffer[photonID]);

				Spectrum f;

				switch (matType) {

				case MAT_MATTE:
					engine->ss->Matte_Sample_f(&hitPointMat->param.matte, &wo,
							&wi, &fPdf, &f, &shadeN, u0, u1, &specularBounce);

					f *= surfaceColor;
					break;

				case MAT_MIRROR:
					engine->ss->Mirror_Sample_f(&hitPointMat->param.mirror, &wo,
							&wi, &fPdf, &f, &shadeN, &specularBounce);
					f *= surfaceColor;
					break;

				case MAT_GLASS:
					engine->ss->Glass_Sample_f(&hitPointMat->param.glass, &wo,
							&wi, &fPdf, &f, &N, &shadeN, u0, &specularBounce);
					f *= surfaceColor;

					break;

				case MAT_MATTEMIRROR:
					engine->ss->MatteMirror_Sample_f(
							&hitPointMat->param.matteMirror, &wo, &wi, &fPdf,
							&f, &shadeN, u0, u1, u2, &specularBounce);
					f *= surfaceColor;

					break;

				case MAT_METAL:
					engine->ss->Metal_Sample_f(&hitPointMat->param.metal, &wo,
							&wi, &fPdf, &f, &shadeN, u0, u1, &specularBounce);
					f *= surfaceColor;

					break;

				case MAT_MATTEMETAL:
					engine->ss->MatteMetal_Sample_f(
							&hitPointMat->param.matteMetal, &wo, &wi, &fPdf, &f,
							&shadeN, u0, u1, u2, &specularBounce);
					f *= surfaceColor;

					break;

				case MAT_ALLOY:
					engine->ss->Alloy_Sample_f(&hitPointMat->param.alloy, &wo,
							&wi, &fPdf, &f, &shadeN, u0, u1, u2,
							&specularBounce);
					f *= surfaceColor;

					break;

				case MAT_ARCHGLASS:
					engine->ss->ArchGlass_Sample_f(
							&hitPointMat->param.archGlass, &wo, &wi, &fPdf, &f,
							&N, &shadeN, u0, &specularBounce);
					f *= surfaceColor;

					break;

				case MAT_NULL:
					wi = currentRay.d;
					specularBounce = 1;
					fPdf = 1.f;
					break;

				default:
					// Huston, we have a problem...
					specularBounce = 1;
					fPdf = 0.f;
					break;
				}

				if (!specularBounce) { // if difuse
					lookupA->AddFlux(hitPoint, shadeN, -currentRay.d,
							currentPhotonPath.flux, currentPhotonRadius2,
							GetHitPoint(0), ss);
					AtomicInc(photonHitCount);

//					printf("%d %.5f %.5f %.5f\n",*photonHitCount,
//							hitPoint.x, hitPoint.y,
//							hitPoint.z);

				}

				if (currentPhotonPath.depth < (MAX_PHOTON_PATH_DEPTH)) {
					// Build the next vertex path ray
					if ((fPdf <= 0.f) || f.Black()) {
						currentPhotonPath.done = true;
					} else {
						currentPhotonPath.depth++;
						currentPhotonPath.flux *= f / fPdf;

						// Russian Roulette
						const float p = 0.75f;
						if (currentPhotonPath.depth < 3) {
							currentRay = Ray(hitPoint, wi);
						} else if (getFloatRNG(seedBuffer[photonID]) < p) {
							currentPhotonPath.flux /= p;
							currentRay = Ray(hitPoint, wi);
						} else {
							currentPhotonPath.done = true;

						}
					}
				} else {
					currentPhotonPath.done = true;
				}
			}
		}

		if (currentPhotonPath.done) {

			//printf("depth %u\n", currentPhotonPath.depth);

			if (*donePhotonCount < photonTarget) {

				photonID = AtomicInc(donePhotonCount);
				currentRay = Ray();
				currentPhotonPath = PhotonPath();
				rayHit;
				rayHit.SetMiss();

				engine->InitPhotonPath(engine->ss, &currentPhotonPath,
						&currentRay, seedBuffer[photonID]);

			} else
				photonID = -1;
		}
	}

}

u_int64_t CPU_Worker::BuildPhotonMap(const u_int64_t photonTarget) {

	double start = WallClockTime();

	uint photonHitCount = 0;
	uint donePhotonCount = 0;
	uint rayCount[NUM_THREADS];
	uint totalRays = 0;

	__p.lsstt("Process Iterations > Iterations > Build Photon Map > Trace", 1);

	__p.lsstp("Process Iterations > Iterations > Build Photon Map > Trace", 1);

	__p.lsstt(
			"Process Iterations > Iterations > Build Photon Map > Sort pho hits",
			1);

	__p.lsstt(
			"Process Iterations > Iterations > Build Photon Map > Sort pho hits > bb",
			1);
	__p.lsstp(
			"Process Iterations > Iterations > Build Photon Map > Sort pho hits > bb",
			1);

	__p.lsstt(
			"Process Iterations > Iterations > Build Photon Map > Sort pho hits > sort",
			1);
	__p.lsstp(
			"Process Iterations > Iterations > Build Photon Map > Sort pho hits > sort",
			1);

	__p.lsstt(
			"Process Iterations > Iterations > Build Photon Map > Sort pho hits > move",
			1);
	__p.lsstp(
			"Process Iterations > Iterations > Build Photon Map > Sort pho hits > move",
			1);

	__p.lsstp(
			"Process Iterations > Iterations > Build Photon Map > Sort pho hits",
			1);

	__p.lsstt("Process Iterations > Iterations > Build Photon Map > Search", 1);

	__p.lsstp("Process Iterations > Iterations > Build Photon Map > Search", 1);

#pragma omp parallel num_threads(NUM_THREADS)
	{

		int tid = omp_get_thread_num();
		//Bind(tid);
		photonTraceEntry(this, &donePhotonCount, &rayCount[tid],
				&photonHitCount, photonTarget);
	}

//	boost::thread* threads[NUM_THREADS];
//	for (int th = 0; th < NUM_THREADS; th++) {
//		threads[th] = new boost::thread(
//				boost::bind(&photonTraceEntry, this, &donePhotonCount,
//						&rayCount[th], &photonHitCount, photonTarget));
//	}

//	for (int th = 0; th < NUM_THREADS; th++) {
//		threads[th]->start_thread();
//	}

	for (int th = 0; th < NUM_THREADS; th++) {
		//threads[th]->join();
		totalRays += rayCount[th];

	}

	profiler->addPhotonTracingTime(WallClockTime() - start);

	profiler->addPhotonsTraced(donePhotonCount);
	profiler->addphotonRaysTraced(totalRays);

//	float MPhotonsSec = donePhotonCount
//			/ ((WallClockTime() - start) * 1000000.f);

//	printf("Photons traced %u\n", donePhotonCount);
//	printf("Photon hit count %u\n", photonHitCount);

	//printf("CPU Photon contributed %u\n", lookupA->call_times);

	lookupA->call_times = 0;

	return donePhotonCount;
}

//u_int64_t CPU_Worker::BuildPhotonMap(u_int64_t photonTarget) {
//
//	double start = WallClockTime();
//
//	HitPoint* hp = GetHitPoint(0);
//
//	uint todoPhotonCount = 0;
//	unsigned long photonHitCount = 0;
//
//	PhotonPath* livePhotonPaths = new PhotonPath[GetWorkSize()];
//
//	rayBuffer->Reset();
//
//	//size_t initc = min(rayBuffer->GetSize(), photonTarget);
//	size_t initc = (
//			((rayBuffer->GetSize()) < (photonTarget)) ?
//					(rayBuffer->GetSize()) : (photonTarget));
//
//	for (size_t i = 0; i < initc; ++i) {
//
//		int p = rayBuffer->ReserveRay();
//
//		Ray * b = &(rayBuffer->GetRayBuffer())[p];
//
//		engine->InitPhotonPath(engine->ss, &livePhotonPaths[i], b,
//				seedBuffer[i]);
//
//		__sync_fetch_and_add(&photonHitCount, 1);
//		printf("Photon depth %lu\n",  photonHitCount);
//
//
//	}
//
//	while (todoPhotonCount < photonTarget) {
//
//		Intersect(rayBuffer);
//
//#ifndef __DEBUG
//		omp_set_num_threads(NUM_THREADS);
//#pragma omp parallel for schedule(guided)
//#endif
//		for (unsigned int i = 0; i < rayBuffer->GetRayCount(); ++i) {
//
//			PhotonPath *photonPath = &livePhotonPaths[i];
//			Ray *ray = &rayBuffer->GetRayBuffer()[i];
//			RayHit *rayHit = &rayBuffer->GetHitBuffer()[i];
//
//			if (photonPath->done == true) {
//				continue;
//			}
//
//			if (rayHit.Miss()) {
//				photonPath->done = true;
//			} else { // Something was hit
//
//				Point hitPoint;
//				Spectrum surfaceColor;
//				Normal N, shadeN;
//
//				if (engine->GetHitPointInformation(engine->ss, ray, rayHit,
//						hitPoint, surfaceColor, N, shadeN))
//					continue;
//
//				const unsigned int currentTriangleIndex = rayHit.index;
//				const unsigned int currentMeshIndex =
//						engine->ss->meshIDs[currentTriangleIndex];
//
//				POINTERFREESCENE::Material *hitPointMat =
//						&engine->ss->materials[engine->ss->meshMats[currentMeshIndex]];
//
//				uint matType = hitPointMat->type;
//				if (matType == MAT_AREALIGHT) {
//
//					photonPath->done = true;
//				} else {
//
//					float fPdf;
//					Vector wi;
//					Vector wo = -ray->d;
//					bool specularBounce = true;
//
//					float u0 = getFloatRNG(seedBuffer[i]);
//					float u1 = getFloatRNG(seedBuffer[i]);
//					float u2 = getFloatRNG(seedBuffer[i]);
//
//					Spectrum f;
//
//					switch (matType) {
//
//					case MAT_MATTE:
//						engine->ss->Matte_Sample_f(&hitPointMat->param.matte,
//								&wo, &wi, &fPdf, &f, &shadeN, u0, u1,
//								&specularBounce);
//
//						f *= surfaceColor;
//						break;
//
//					case MAT_MIRROR:
//						engine->ss->Mirror_Sample_f(&hitPointMat->param.mirror,
//								&wo, &wi, &fPdf, &f, &shadeN, &specularBounce);
//						f *= surfaceColor;
//						break;
//
//					case MAT_GLASS:
//						engine->ss->Glass_Sample_f(&hitPointMat->param.glass,
//								&wo, &wi, &fPdf, &f, &N, &shadeN, u0,
//								&specularBounce);
//						f *= surfaceColor;
//
//						break;
//
//					case MAT_MATTEMIRROR:
//						engine->ss->MatteMirror_Sample_f(
//								&hitPointMat->param.matteMirror, &wo, &wi,
//								&fPdf, &f, &shadeN, u0, u1, u2,
//								&specularBounce);
//						f *= surfaceColor;
//
//						break;
//
//					case MAT_METAL:
//						engine->ss->Metal_Sample_f(&hitPointMat->param.metal,
//								&wo, &wi, &fPdf, &f, &shadeN, u0, u1,
//								&specularBounce);
//						f *= surfaceColor;
//
//						break;
//
//					case MAT_MATTEMETAL:
//						engine->ss->MatteMetal_Sample_f(
//								&hitPointMat->param.matteMetal, &wo, &wi, &fPdf,
//								&f, &shadeN, u0, u1, u2, &specularBounce);
//						f *= surfaceColor;
//
//						break;
//
//					case MAT_ALLOY:
//						engine->ss->Alloy_Sample_f(&hitPointMat->param.alloy,
//								&wo, &wi, &fPdf, &f, &shadeN, u0, u1, u2,
//								&specularBounce);
//						f *= surfaceColor;
//
//						break;
//
//					case MAT_ARCHGLASS:
//						engine->ss->ArchGlass_Sample_f(
//								&hitPointMat->param.archGlass, &wo, &wi, &fPdf,
//								&f, &N, &shadeN, u0, &specularBounce);
//						f *= surfaceColor;
//
//						break;
//
//					case MAT_NULL:
//						wi = ray->d;
//						specularBounce = 1;
//						fPdf = 1.f;
//						break;
//
//					default:
//						// Huston, we have a problem...
//						specularBounce = 1;
//						fPdf = 0.f;
//						break;
//					}
//
//					if (!specularBounce) { // if difuse
//						lookupA->AddFlux(hitPoint, shadeN, -ray->d,
//								photonPath->flux, currentPhotonRadius2,
//								GetHitPoint(0), ss);
//
//
//					}
//
//					if (photonPath->depth < (MAX_PHOTON_PATH_DEPTH )) {
//						// Build the next vertex path ray
//						if ((fPdf <= 0.f) || f.Black()) {
//							photonPath->done = true;
//						} else {
//							photonPath->depth++;
//							photonPath->flux *= f / fPdf;
//
//							// Russian Roulette
//							const float p = 0.75f;
//							if (photonPath->depth < 3) {
//								*ray = Ray(hitPoint, wi);
//							} else if (getFloatRNG(seedBuffer[i]) < p) {
//								photonPath->flux /= p;
//								*ray = Ray(hitPoint, wi);
//							} else {
//								photonPath->done = true;
//
//
//							}
//						}
//					} else {
//						photonPath->done = true;
//					}
//				}
//			}
//		}
//
//		uint oldc = rayBuffer->GetRayCount();
//
//		rayBuffer->Reset();
//
//		for (unsigned int i = 0; i < oldc; ++i) {
//
//			PhotonPath *photonPath = &livePhotonPaths[i];
//			Ray *ray = &rayBuffer->GetRayBuffer()[i];
//
//			if (photonPath->done && todoPhotonCount < photonTarget) {
//				todoPhotonCount++;
//
//				Ray n;
//				engine->InitPhotonPath(engine->ss, photonPath, &n,
//						seedBuffer[i]);
//
//				livePhotonPaths[i].done = false;
//
//				size_t p = rayBuffer->AddRay(n);
//				livePhotonPaths[p] = *photonPath;
//
//			} else if (!photonPath->done) {
//				rayBuffer->AddRay(*ray);
//			}
//		}
//	}
//
//	printf("Photon hit count %lu\n", photonHitCount);
//
//
//	profiler->addPhotonTracingTime(WallClockTime() - start);
//	profiler->addPhotonsTraced(todoPhotonCount);
//
//	rayBuffer->Reset();
//
//	delete[] livePhotonPaths;
//
//	float MPhotonsSec = todoPhotonCount
//			/ ((WallClockTime() - start) * 1000000.f);
//
//	//fprintf(stderr, "Device %d: Photon mapping MPhotons/sec: %.3f\n", deviceID,
//	//		MPhotonsSec);
//
//	printf("CPU Photon contributed %u\n", lookupA->call_times);
//	lookupA->call_times = 0;
//
//	return todoPhotonCount;
//}

void CPU_Worker::updateDeviceHitPointsFlux() {

}

void CPU_Worker::updateDeviceHitPointsInfo(bool toHost) {
	if (toHost)
		memcpy(cfg->GetEngine()->GetHitPoints(), HPsPositionInfo,
				sizeof(HitPoint) * cfg->GetHitPointTotal());
	else
		memcpy(HPsPositionInfo, cfg->GetEngine()->GetHitPoints(),
				sizeof(HitPoint) * cfg->GetHitPointTotal());
}

//void CPU_Worker::ResetDeviceHitPointsFlux() {
//	memset(HPsIterationRadianceFlux, 0,
//			sizeof(HitPointRadianceFlux) * cfg->hitPointTotal);
//}

void CPU_Worker::ResetDeviceHitPointsInfo() {
	memset(HPsPositionInfo, 0, sizeof(HitPoint) * cfg->GetHitPointTotal());
}

void CPU_Worker::AccumulateFluxPPMPA(uint iteration, u_int64_t photonTraced) {

#ifndef __DEBUG
	omp_set_num_threads(NUM_THREADS);
#pragma omp parallel for schedule(guided)
#endif
	for (uint i = 0; i < cfg->GetHitPointTotal(); i++) {

		//Bind(omp_get_thread_num());

		HitPoint *ihp = GetHitPoint(i);
		//HitPointRadianceFlux *ihp = GetHitPoint(i);

		ihp->radiance = Spectrum();

		switch (ihp->type) {
		case CONSTANT_COLOR:
			ihp->radiance = ihp->throughput;

			break;
		case SURFACE:

			if ((ihp->accumPhotonCount > 0)) {

				ihp->reflectedFlux = ihp->accumReflectedFlux;

				//out of the loop
				const double k = 1.0
						/ (M_PI * currentPhotonRadius2 * photonTraced);

				ihp->radiance = ihp->reflectedFlux * k;

			}
			break;
		default:
			assert(false);
		}

		ihp->accumPhotonCount = 0;
		ihp->accumReflectedFlux = Spectrum();

	}
}

void CPU_Worker::AccumulateFluxPPM(uint iteration, u_int64_t photonTraced) {

	photonTraced += engine->getPhotonTracedTotal();

#ifndef __DEBUG
	omp_set_num_threads(NUM_THREADS);
#pragma omp parallel for schedule(guided)
#endif
	for (uint i = 0; i < cfg->GetHitPointTotal(); i++) {
		//Bind(omp_get_thread_num());

		HitPoint *ihp = GetHitPoint(i);
		//HitPointRadianceFlux *ihp = GetHitPoint(i);

		switch (ihp->type) {
		case CONSTANT_COLOR:
			ihp->radiance = ihp->throughput;
			break;
		case SURFACE:

			if ((ihp->accumPhotonCount > 0)) {

				const unsigned long long pcount = ihp->photonCount
						+ ihp->accumPhotonCount;
				const float alpha = cfg->alpha;

				const float g = alpha * pcount
						/ (ihp->photonCount * alpha + ihp->accumPhotonCount);

				ihp->accumPhotonRadius2 *= g;

				ihp->reflectedFlux = (ihp->reflectedFlux
						+ ihp->accumReflectedFlux) * g;

				ihp->photonCount = pcount;

				const double k = 1.0
						/ (M_PI * ihp->accumPhotonRadius2 * photonTraced);

				ihp->radiance = ihp->reflectedFlux * k;

				ihp->accumPhotonCount = 0;
				ihp->accumReflectedFlux = Spectrum();
			}

			break;
		default:
			assert(false);
		}

	}

//fprintf(stderr, "Iteration %d hit point 0 reducted radius: %f\n", iteration,
//		GetHitPoint(0)->accumPhotonRadius2);
}

void CPU_Worker::AccumulateFluxSPPM(uint iteration, u_int64_t photonTraced) {

	photonTraced += engine->getPhotonTracedTotal();

#ifndef __DEBUG
	omp_set_num_threads(NUM_THREADS);
#pragma omp parallel for schedule(guided)
#endif
	for (uint i = 0; i < cfg->GetHitPointTotal(); i++) {
		//Bind(omp_get_thread_num());

		HitPoint *ihp = GetHitPoint(i);
		//HitPointRadianceFlux *ihp = GetHitPoint(i);

		switch (ihp->type) {
		case CONSTANT_COLOR:
			ihp->accumRadiance += ihp->throughput;
			ihp->constantHitsCount += 1;
			break;

		case SURFACE:

			if ((ihp->accumPhotonCount > 0)) {

				const unsigned long long pcount = ihp->photonCount
						+ ihp->accumPhotonCount;
				const float alpha = cfg->alpha;

				const float g = alpha * pcount
						/ (ihp->photonCount * alpha + ihp->accumPhotonCount);

				ihp->accumPhotonRadius2 *= g;

				ihp->reflectedFlux = (ihp->reflectedFlux
						+ ihp->accumReflectedFlux) * g;

				ihp->photonCount = pcount;

				ihp->accumPhotonCount = 0;
				ihp->accumReflectedFlux = Spectrum();
			}

			ihp->surfaceHitsCount += 1;
			break;
		default:
			assert(false);
		}
		const unsigned int hitCount = ihp->constantHitsCount
				+ ihp->surfaceHitsCount;
//		if (hitCount > 0) {
//
//			const double k = 1.0
//					/ (M_PI * ihp->accumPhotonRadius2 * photonTraced);
//			Spectrum radiance_r;
//			radiance_r = (ihp->radiance
//					+ ihp->surfaceHitsCount * ihp->reflectedFlux * k)
//					/ hitCount;
//			ihp->radiance = radiance_r;
//		}

		if (hitCount > 0) {
			const double k = 1.0
					/ (M_PI * ihp->accumPhotonRadius2 * photonTraced);
			ihp->radiance = (ihp->accumRadiance
					+ ihp->surfaceHitsCount * ihp->reflectedFlux * k)
					/ hitCount;
		}

	}

//fprintf(stderr, "Iteration %d hit point 0 reducted radius: %f\n", iteration,
//		GetHitPoint(0)->accumPhotonRadius2);
}

void CPU_Worker::AccumulateFluxSPPMPA(uint iteration, u_int64_t photonTraced) {

#ifndef __DEBUG
	omp_set_num_threads(NUM_THREADS);
#pragma omp parallel for schedule(guided)
#endif
	for (uint i = 0; i < cfg->GetHitPointTotal(); i++) {
		//Bind(omp_get_thread_num());
		HitPoint *ihp = GetHitPoint(i);
		//HitPointRadianceFlux *ihp = GetHitPoint(i);

		switch (ihp->type) {
		case CONSTANT_COLOR:
			ihp->accumRadiance += ihp->throughput;
			ihp->constantHitsCount += 1;
			break;
		case SURFACE:

			if ((ihp->accumPhotonCount > 0)) {

				ihp->reflectedFlux = ihp->accumReflectedFlux;
				ihp->accumPhotonCount = 0;
				ihp->accumReflectedFlux = Spectrum();
			}
			ihp->surfaceHitsCount += 1;
			break;
		default:
			assert(false);
		}

		const unsigned int hitCount = ihp->constantHitsCount
				+ ihp->surfaceHitsCount;

//		if (hitCount > 0) {
//
//			const double k = 1.0 / (M_PI * currentPhotonRadius2 * photonTraced);
//
//			ihp->radiance = (ihp->accumRadiance + ihp->reflectedFlux * k);
//
//		}

		if (hitCount > 0) {
			const double k = 1.0 / (M_PI * currentPhotonRadius2 * photonTraced);
			ihp->radiance = (ihp->accumRadiance
					+ ihp->surfaceHitsCount * ihp->reflectedFlux * k)
					/ hitCount;
		}

	}

//	for (uint i = 0; i < engine->hitPointTotal; i++) {
//		HitPointRadianceFlux *ihp = GetHitPoint(i);
//
//		ihp->constantHitsCount = 0;
//		ihp->surfaceHitsCount = 0;
//		ihp->accumRadiance = Spectrum();
//	}

}

void CPU_Worker::SetNonPAInitialRadius2(float photonRadius2) {

	for (unsigned int i = 0; i < cfg->GetHitPointTotal(); ++i) {

		HitPoint *hp = GetHitPoint(i);

		hp->accumPhotonRadius2 = photonRadius2;

	}
}

float CPU_Worker::GetNonPAMaxRadius2() {
	float maxPhotonRadius2 = 0.f;
	for (unsigned int i = 0; i < cfg->GetHitPointTotal(); ++i) {

		HitPoint *ihp = &HPsPositionInfo[i];
		//HitPointRadianceFlux *hp = &HPsIterationRadianceFlux[i];

		if (ihp->type == SURFACE)
			maxPhotonRadius2 = max(maxPhotonRadius2, ihp->accumPhotonRadius2);
	}

	return maxPhotonRadius2;
}

void CPU_Worker::GetSampleBuffer() {

	__p.lsstt(
			"Process Iterations > Iterations > Update Samples > HP to sample");

#ifndef __DEBUG
	omp_set_num_threads(NUM_THREADS);
#pragma omp parallel for schedule(guided)
#endif
	for (unsigned int i = 0; i < cfg->GetHitPointTotal(); ++i) {
		//Bind(omp_get_thread_num());
		HitPoint *hp = GetHitPoint(i);
		//HitPointRadianceFlux *ihp = GetHitPoint(i);

		sampleBuffer->SplatSample(hp->scrX, hp->scrY, hp->radiance, i, hp->id);

	}

	__p.lsstp(
			"Process Iterations > Iterations > Update Samples > HP to sample");

	__p.lsstt(
			"Process Iterations > Iterations > Update Samples > Copy samples RGB");

	__p.lsstp(
			"Process Iterations > Iterations > Update Samples > Copy samples RGB");

}
