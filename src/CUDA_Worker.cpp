/*
 * CUDA_Worker.cpp
 *
 *  Created on: Oct 31, 2012
 *      Author: rr
 */

#include "CUDA_Worker.h"
#include "omp.h"

CUDA_Worker::~CUDA_Worker() {

}

void CUDA_Worker::Render(bool buildHitPoints) {

	cudaSetDevice(deviceID);

	CUDPPResult err;
	err = cudppCreate(&theCudpp);
	assert(err == CUDPP_SUCCESS);
	//reduceConfig.datatype = CUDPP_FLOAT;
	//reduceConfig.algorithm = CUDPP_REDUCE;

	if (cfg->GPU_AS == GPU_HASH_GRID)
		lookupA = new GPUHashGrid(cfg->GetHitPointTotal() * 2, theCudpp,
				cfg->rebuildHash);

	//lookupA = new GPUHashGrid(1 << (3 * MORTON_BITS), cfg->rebuildHash);

	else if (cfg->GPU_AS == GPU_MORTON_HASH_GRID)
		lookupA = new GPUMortonHashGrid(1 << (3 * MORTON_BITS),theCudpp,
				cfg->rebuildHash);
	//lookupA = new GPUMortonHashGrid(cfg->hitPointTotal, cfg->rebuildHash);

	else if (cfg->GPU_AS == GPU_MORTON_GRID)
		lookupA = new GPUMortonGrid(MORTON_BITS);

	AllocCUDABuffer((void**) &rayTraceCountBuff, sizeof(unsigned long long));
	AllocCUDABuffer((void**) (&photonCountBuff),
			sizeof(unsigned long long int));
	AllocCUDABuffer((void**) (&workerBuff), sizeof(CUDA_Worker));
	AllocCUDABuffer((void**) (&ssBuff), sizeof(PointerFreeScene));
	AllocCUDABuffer((void**) &photonHitCountBuff, sizeof(unsigned long long));
	AllocCUDABuffer((void**) (&workerHitPointsInfoBuff),
			sizeof(HitPoint) * cfg->GetHitPointTotal());

	AllocCUDABuffer((void**) (&sampleBufferBuff),
			cfg->GetHitPointTotal() * sizeof(SampleBufferElem));

	AllocCUDABuffer((void**) (&seedsBuff), liveSeeds * sizeof(Seed));

	//uint hit_power_of_two = upper_power_of_two(cfg->GetHitPointTotal());
	//int numBlocks, numThreads;
	//ComputeGridSize(hit_power_of_two, 1024, numBlocks, numThreads);

	//boilBuff_size = sizeof(float) * hit_power_of_two;

	//AllocCUDABuffer((void**) &bbox_boilBuff,  sizeof(float) );

	//cudaMemset(bbox_boilBuff, 0, boilBuff_size);

	AllocCUDABuffer((void**) &bbox_outBuff, sizeof(float));
	cudaMemset(bbox_outBuff, 0, sizeof(float));

	cudaMemset(workerHitPointsInfoBuff, 0,
			sizeof(HitPoint) * cfg->GetHitPointTotal());

	lookupA->Init();

	CopyAcc();
	CopyGeometryToDevices();

	checkCUDAError((char*) "Render");

	size_t free, total;
	cudaMemGetInfo(&free, &total);
	free -= 0.5 * free;

	float n = double(
			(cfg->photonsFirstIteration * MAX_PHOTON_PATH_DEPTH
					* (sizeof(PhotonHit) + 30))) / double((free));

	n = upper_power_of_two(ceil(n));

	fprintf(stderr, "Slicing Photons in|%.f...\n", n);

	cfg->photons_per_slice = floor(cfg->photonsFirstIteration / n);

	cfg->PHOTON_HIT_BUFFER_SIZE =
			cfg->photons_per_slice * MAX_PHOTON_PATH_DEPTH;

	AllocCUDABuffer((void**) &photonHitsBuff,
			sizeof(PhotonHit) * cfg->PHOTON_HIT_BUFFER_SIZE);

	SetSceneAndWorkerPointer(this, engine);

	GenerateSeedBuffer_wrapper(this);

	checkCUDAmemory((char*) "Process Iterations");

	__p.reg("Process Iterations");

	engine->ProcessIterations(this, buildHitPoints);

	__p.stp("Process Iterations");

	__E(cudaFree(photonCountBuff));
	//__E(cudaFree(engineBuff));
	__E(cudaFree(ssBuff));
	__E(cudaFree(workerBuff));

}

void CUDA_Worker::ProcessEyePaths() {

	double start = WallClockTime();

	uint r = BuildHitpoints_wrapper(this);

	profiler->addRayTracingTime(WallClockTime() - start);
	profiler->addEyesamplesRaysTraced(r);
	profiler->addEyesamplesTraced(cfg->GetHitPointTotal());

}

void CUDA_Worker::UpdateBBox() {

	BBox hitPointsbbox = BBox();

	updateBBox_wrapper(this, hitPointsbbox);

//	hitPointsbbox = BBox();
//
//	// Calculate hit points bounding box
//	//std::cerr << "Building hit points bounding box: ";
//
//
//	 HitPoint* m = (HitPoint*)malloc( sizeof(HitPoint) * cfg->GetHitPointTotal());
//
//	cudaMemcpy(m, workerHitPointsInfoBuff,
//			sizeof(HitPoint) * cfg->GetHitPointTotal(),
//			cudaMemcpyDeviceToHost);
//
//	for (unsigned int i = 0; i < cfg->GetHitPointTotal(); ++i) {
//		HitPoint *hp = &m[i];
//
//		if (hp->type == SURFACE)
//			hitPointsbbox = Union(hitPointsbbox, hp->position);
//	}
//
//
//	printf("%.10f\n", hitPointsbbox.pMin.x);
//	printf("%.10f\n", hitPointsbbox.pMin.y);
//	printf("%.10f\n", hitPointsbbox.pMin.z);
//	printf("%.10f\n", hitPointsbbox.pMax.x);
//	printf("%.10f\n", hitPointsbbox.pMax.y);
//	printf("%.10f\n", hitPointsbbox.pMax.z);

	SetBBox(hitPointsbbox);

}

float CUDA_Worker::GetNonPAMaxRadius2() {

	return GetNonPAMaxRadius2_wrapper(this);

}

void CUDA_Worker::SetNonPAInitialRadius2(float photonRadius2) {
	SetNonPAInitialRadius2_wrapper(this, photonRadius2);
}

u_int64_t CUDA_Worker::BuildPhotonMap(u_int64_t iterationPhotonTarget) {

	double start = WallClockTime();

	uint slices = IntDivUp(iterationPhotonTarget, cfg->photons_per_slice);

	uint photonTarget = cfg->photons_per_slice;


	for (int i = 0; i < slices; i++) {

		if (i == (slices - 1))
			photonTarget = iterationPhotonTarget - (i * cfg->photons_per_slice);

		unsigned long long photonHitCount = BuildPhotonMap_wrapper(this, engine,
				photonTarget);

		lookupA->LookupPhotonHits(photonHitCount, currentPhotonRadius2);


//		PhotonHit* eyePoints = new PhotonHit[photonHitCount];
//			cudaMemcpy(eyePoints, photonHitsBuff,
//					photonHitCount * sizeof(PhotonHit), cudaMemcpyDeviceToHost);
//
//			for (int i = 0; i < photonHitCount; i++) {
//				printf("%d %.5f %.5f %.5f\n", i, eyePoints[i].hitPoint.x,
//						eyePoints[i].hitPoint.y, eyePoints[i].hitPoint.z);
//			}

	}

	float MPhotonsSec = iterationPhotonTarget
			/ ((WallClockTime() - start) * 1000000.f);

	//fprintf(stderr, "Device %d: Photon mapping MPhotons/sec: %.3f\n", deviceID,
	//		MPhotonsSec);



	return iterationPhotonTarget;
}

void CUDA_Worker::AccumulateFluxPPM(uint iteration, u_int64_t photonTraced) {

	photonTraced += engine->getPhotonTracedTotal();

	AccumulateFluxPPM_wrapper(this, photonTraced);
}

void CUDA_Worker::AccumulateFluxSPPM(uint iteration, u_int64_t photonTraced) {

	photonTraced += engine->getPhotonTracedTotal();

	AccumulateFluxSPPM_wrapper(this, photonTraced);

//	fprintf(stderr, "Iteration %d hit point 0 reducted radius: %f\n", iteration,
//			GetHitPoint(0)->accumPhotonRadius2);
}

void CUDA_Worker::AccumulateFluxPPMPA(uint iteration, u_int64_t photonTraced) {
	AccumulateFluxPPMPA_wrapper(this, photonTraced);
}

void CUDA_Worker::AccumulateFluxSPPMPA(uint iteration, u_int64_t photonTraced) {

	AccumulateFluxSPPMPA_wrapper(this, photonTraced);

}

void CUDA_Worker::MirrorHitPoints() {
	//
	//	engine->lockHitPoints();
	//
	//	for (uint i = 0; i < engine->hitPointTotal; i++) {
	//		HitPoint *ihp = &(engine->hitPoints)[i];
	//		IterationHitPoint *hp = &(iterationHitPoints)[i];
	//
	//		hp->radiance = ihp->radiance;
	//		hp->accumRadiance = ihp->accumRadiance;
	//		hp->photonCount = ihp->photonCount;
	//		hp->reflectedFlux = ihp->reflectedFlux;
	//		hp->constantHitsCount = ihp->constantHitsCount;
	//		hp->surfaceHitsCount = ihp->surfaceHitsCount;
	//		hp->accumPhotonRadius2 = ihp->accumPhotonRadius2;
	//
	//	}
	//
	//	engine->unlockHitPoints();
	//
}

void CUDA_Worker::CommitIterationHitPoints(u_int64_t photonPerIteration) {
	//
	//	engine->lockHitPoints();
	//
	//	for (uint i = 0; i < engine->hitPointTotal; i++) {
	//		HitPoint *hp = &(engine->hitPoints)[i];
	//		IterationHitPoint *ihp = &(iterationHitPoints)[i];
	//
	//		hp->radiance = ihp->radiance;
	//		hp->accumRadiance = ihp->accumRadiance;
	//		hp->photonCount = ihp->photonCount;
	//		hp->reflectedFlux = ihp->reflectedFlux;
	//		hp->constantHitsCount = ihp->constantHitsCount;
	//		hp->surfaceHitsCount = ihp->surfaceHitsCount;
	//		hp->accumPhotonRadius2 = ihp->accumPhotonRadius2;
	//
	//	}
	//
	//	engine->incPhotonCount(photonPerIteration);
	//
	//	engine->unlockHitPoints();
	//
}

void CUDA_Worker::getDeviceHitpoints() {

//	cudaMemcpy(HPsPositionInfo, workerHitPointsInfoBuff,
//			sizeof(HitPointPositionInfo) * cfg->hitPointTotal,
//			cudaMemcpyDeviceToHost);
//
//	cudaMemcpy(HPsIterationRadianceFlux, workerHitPointsBuff,
//			sizeof(HitPointRadianceFlux) * cfg->hitPointTotal,
//			cudaMemcpyDeviceToHost);

}

void CUDA_Worker::updateDeviceHitPointsInfo(bool toHost) {

	if (toHost)
		cudaMemcpy(cfg->GetEngine()->GetHitPoints(), workerHitPointsInfoBuff,
				sizeof(HitPoint) * cfg->GetHitPointTotal(),
				cudaMemcpyDeviceToHost);
	else
		cudaMemcpy(workerHitPointsInfoBuff, cfg->GetEngine()->GetHitPoints(),
				sizeof(HitPoint) * cfg->GetHitPointTotal(),
				cudaMemcpyHostToDevice);

}

void CUDA_Worker::ResetDeviceHitPointsInfo() {
	cudaMemset(workerHitPointsInfoBuff, 0,
			sizeof(HitPoint) * cfg->GetHitPointTotal());
}

void CUDA_Worker::CopyAcc() {

	POINTERFREESCENE::QBVHNode *nodes =
			(POINTERFREESCENE::QBVHNode *) ss->dataSet->GetAccelerator()->GetNodes();
	uint nNodes = ss->dataSet->GetAccelerator()->GetNodesCount();
	POINTERFREESCENE::QuadTriangle *prims =
			(POINTERFREESCENE::QuadTriangle *) ss->dataSet->GetAccelerator()->GetPrims();

	uint nQuads = ss->dataSet->GetAccelerator()->GetPrimsCount();

	//	qbvhBuff = new cl::Buffer(oclContext,
	//			CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
	//			sizeof(QBVHNode) * qbvh->nNodes, qbvh->nodes);
	//
	//	qbvhTrisBuff = new cl::Buffer(oclContext,
	//			CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
	//			sizeof(QuadTriangle) * qbvh->nQuads, qbvh->prims);

	//	qbvhKernel->setArg(0, *raysBuff);
	//				qbvhKernel->setArg(1, *hitsBuff);
	//				qbvhKernel->setArg(4, (unsigned int)rayBuffer->GetRayCount());
	//				oclQueue->enqueueNDRangeKernel(*qbvhKernel, cl::NullRange,
	//					cl::NDRange(rayBuffer->GetSize()), cl::NDRange(qbvhWorkGroupSize));

	AllocCUDABuffer((void**) &d_qbvhBuff,
			sizeof(POINTERFREESCENE::QBVHNode) * nNodes);
	AllocCUDABuffer((void**) &d_qbvhTrisBuff,
			sizeof(POINTERFREESCENE::QuadTriangle) * nQuads);

	cudaMemcpy(d_qbvhBuff, nodes, sizeof(POINTERFREESCENE::QBVHNode) * nNodes,
			cudaMemcpyHostToDevice);

	cudaMemcpy(d_qbvhTrisBuff, prims,
			sizeof(POINTERFREESCENE::QuadTriangle) * nQuads,
			cudaMemcpyHostToDevice);

}

void CUDA_Worker::InitCamera() {
	AllocCopyCUDABuffer((void**) &cameraBuff, &ss->camera,
			sizeof(POINTERFREESCENE::Camera), "Camera");
}

void CUDA_Worker::InitGeometry() {
	//Scene *scene = renderEngine->renderConfig->scene;
	//CompiledScene *cscene = renderEngine->compiledScene;

	const unsigned int trianglesCount = ss->dataSet->GetTotalTriangleCount();
	AllocCopyCUDABuffer((void**) &meshIDsBuff, (void *) ss->meshIDs,
			sizeof(unsigned int) * trianglesCount, "MeshIDs");

	AllocCopyCUDABuffer((void**) &normalsBuff, &ss->normals[0],
			sizeof(Normal) * ss->normals.size(), "Normals");

	AllocCopyCUDABuffer((void**) &colorsBuff, &ss->colors[0],
			sizeof(Spectrum) * ss->colors.size(), "Colors");

	if (ss->uvs.size() > 0)
		AllocCopyCUDABuffer((void**) &uvsBuff, &ss->uvs[0],
				sizeof(UV) * ss->uvs.size(), "UVs");
	else
		uvsBuff = NULL;

	AllocCopyCUDABuffer((void**) &vertsBuff, &ss->verts[0],
			sizeof(Point) * ss->verts.size(), "Vertices");

	AllocCopyCUDABuffer((void**) &trisBuff, &ss->tris[0],
			sizeof(Triangle) * ss->tris.size(), "Triangles");

	// Check the used accelerator type
	if (ss->dataSet->GetAcceleratorType() == ACCEL_QBVH) {
		// MQBVH geometry must be defined in a specific way.

		AllocCopyCUDABuffer((void**) &meshFirstTriangleOffsetBuff,
				(void *) ss->meshFirstTriangleOffset,
				sizeof(unsigned int) * ss->meshDescs.size(),
				"First mesh triangle offset");

		AllocCopyCUDABuffer((void**) &meshDescsBuff, &ss->meshDescs[0],
				sizeof(POINTERFREESCENE::Mesh) * ss->meshDescs.size(),
				"Mesh description");
	} else {
		meshFirstTriangleOffsetBuff = NULL;
		meshDescsBuff = NULL;
	}
}

void CUDA_Worker::InitMaterials() {
	const size_t materialsCount = ss->materials.size();
	AllocCopyCUDABuffer((void**) &materialsBuff, &ss->materials[0],
			sizeof(POINTERFREESCENE::Material) * materialsCount, "Materials");

	const unsigned int meshCount = ss->meshMats.size();
	AllocCopyCUDABuffer((void**) &meshMatsBuff, &ss->meshMats[0],
			sizeof(unsigned int) * meshCount, "Mesh material index");
}

void CUDA_Worker::InitAreaLights() {

	if (ss->areaLights.size() > 0) {
		AllocCopyCUDABuffer((void**) &areaLightsBuff, &ss->areaLights[0],
				sizeof(POINTERFREESCENE::TriangleLight) * ss->areaLights.size(),
				"AreaLights");
	} else
		areaLightsBuff = NULL;
}

void CUDA_Worker::InitInfiniteLight() {

	if (ss->infiniteLight) {
		AllocCopyCUDABuffer((void**) &infiniteLightBuff, ss->infiniteLight,
				sizeof(POINTERFREESCENE::InfiniteLight), "InfiniteLight");

		const unsigned int pixelCount = ss->infiniteLight->width
				* ss->infiniteLight->height;
		AllocCopyCUDABuffer((void**) &infiniteLightMapBuff,
				(void *) ss->infiniteLightMap, sizeof(Spectrum) * pixelCount,
				"InfiniteLight map");
	} else {
		infiniteLightBuff = NULL;
		infiniteLightMapBuff = NULL;
	}
}

void CUDA_Worker::InitSunLight() {

	if (ss->sunLight)
		AllocCopyCUDABuffer((void**) &sunLightBuff, ss->sunLight,
				sizeof(POINTERFREESCENE::SunLight), "SunLight");
	else
		sunLightBuff = NULL;
}

void CUDA_Worker::InitSkyLight() {

	if (ss->skyLight)
		AllocCopyCUDABuffer((void**) &skyLightBuff, ss->skyLight,
				sizeof(POINTERFREESCENE::SkyLight), "SkyLight");
	else
		skyLightBuff = NULL;
}

void CUDA_Worker::InitTextureMaps() {

	if ((ss->totRGBTexMem > 0) || (ss->totAlphaTexMem > 0)) {
		if (ss->totRGBTexMem > 0)
			AllocCopyCUDABuffer((void**) &texMapRGBBuff, ss->rgbTexMem,
					sizeof(Spectrum) * ss->totRGBTexMem, "TexMaps");
		else
			texMapRGBBuff = NULL;

		if (ss->totAlphaTexMem > 0)
			AllocCopyCUDABuffer((void**) &texMapAlphaBuff, ss->alphaTexMem,
					sizeof(float) * ss->totAlphaTexMem,
					"TexMaps Alpha Channel");
		else
			texMapAlphaBuff = NULL;

		AllocCopyCUDABuffer((void**) &texMapDescBuff, &ss->gpuTexMaps[0],
				sizeof(POINTERFREESCENE::TexMap) * ss->gpuTexMaps.size(),
				"TexMaps description");

		const unsigned int meshCount = ss->meshMats.size();
		AllocCopyCUDABuffer((void**) &meshTexsBuff, ss->meshTexs,
				sizeof(unsigned int) * meshCount, "Mesh TexMaps index");

		if (ss->meshBumps) {
			AllocCopyCUDABuffer((void**) &meshBumpsBuff, ss->meshBumps,
					sizeof(unsigned int) * meshCount, "Mesh BumpMaps index");

			AllocCopyCUDABuffer((void**) &meshBumpsScaleBuff, ss->bumpMapScales,
					sizeof(float) * meshCount, "Mesh BuSSCENEmpMaps scales");
		} else {
			meshBumpsBuff = NULL;
			meshBumpsScaleBuff = NULL;
		}

		if (ss->meshNormalMaps)
			AllocCopyCUDABuffer((void**) &meshNormalMapsBuff,
					ss->meshNormalMaps, sizeof(unsigned int) * meshCount,
					"Mesh NormalMaps index");
		else
			meshNormalMapsBuff = NULL;
	} else {
		texMapRGBBuff = NULL;
		texMapAlphaBuff = NULL;
		texMapDescBuff = NULL;
		meshTexsBuff = NULL;
		meshBumpsBuff = NULL;
		meshBumpsScaleBuff = NULL;
		meshNormalMapsBuff = NULL;
	}
}

void CUDA_Worker::CopyGeometryToDevices() {

	//--------------------------------------------------------------------------
	// FrameBuffer definition
	//--------------------------------------------------------------------------

	//	InitFrameBuffer();

	//--------------------------------------------------------------------------
	// Camera definition
	//--------------------------------------------------------------------------

	InitCamera();

	//--------------------------------------------------------------------------
	// Scene geometry
	//--------------------------------------------------------------------------

	InitGeometry();

	//--------------------------------------------------------------------------
	// Translate material definitions
	//--------------------------------------------------------------------------

	InitMaterials();

	//--------------------------------------------------------------------------
	// Translate area lights
	//--------------------------------------------------------------------------

	InitAreaLights();

	//--------------------------------------------------------------------------
	// Check if there is an infinite light source
	//--------------------------------------------------------------------------

	InitInfiniteLight();

	//--------------------------------------------------------------------------
	// Check if there is an sun light source
	//--------------------------------------------------------------------------

	InitSunLight();

	//--------------------------------------------------------------------------
	// Check if there is an sky light source
	//--------------------------------------------------------------------------

	InitSkyLight();

	const unsigned int areaLightCount = ss->areaLights.size();
	if (!skyLightBuff && !sunLightBuff && !infiniteLightBuff
			&& (areaLightCount == 0))
		throw runtime_error(
				"There are no light sources supported by PathOCL in the scene");

	//--------------------------------------------------------------------------
	// Translate mesh texture maps
	//--------------------------------------------------------------------------

	InitTextureMaps();

	//--------------------------------------------------------------------------
	// Allocate Ray/RayHit buffers
	//--------------------------------------------------------------------------

	//const unsigned int taskCount = renderEngine->taskCount;

}

void CUDA_Worker::GetSampleBuffer() {

	__p.lsstt(
			"Process Iterations > Iterations > Update Samples > HP to sample");

	UpdadeSampleBuffer_wrapper();

	__p.lsstp(
			"Process Iterations > Iterations > Update Samples > HP to sample");

	__p.lsstt(
			"Process Iterations > Iterations > Update Samples > Copy samples RGB");

	cudaMemcpy(sampleBuffer->samples, sampleBufferBuff,
			sizeof(SampleBufferElem) * cfg->GetHitPointTotal(),
			cudaMemcpyDeviceToHost);

	__p.lsstp(
			"Process Iterations > Iterations > Update Samples > Copy samples RGB");
}

//void CUDA_Worker::IntersectGPU(RayBuffer *rayBuffer) {
//
//	//const double t1 = WallClockTime();
//
//	cudaMemset(hraysBuff, 0, sizeof(RayHit) * rayBuffer->GetRayCount());
//	cudaMemset(raysBuff, 0, sizeof(Ray) * rayBuffer->GetRayCount());
//
//	double start = WallClockTime();
//
//	cudaMemcpy(raysBuff, rayBuffer->GetRayBuffer(),
//			sizeof(Ray) * rayBuffer->GetRayCount(), cudaMemcpyHostToDevice);
//
//	intersect_wrapper(raysBuff, hraysBuff,
//			(POINTERFREESCENE::QBVHNode*) d_qbvhBuff,
//			(POINTERFREESCENE::QuadTriangle*) d_qbvhTrisBuff,
//			rayBuffer->GetRayCount());
//
//	cudaMemcpy(rayBuffer->GetHitBuffer(), hraysBuff,
//			sizeof(RayHit) * rayBuffer->GetRayCount(), cudaMemcpyDeviceToHost);
//
//	profiler->addRayTracingTime(WallClockTime() - start);
//	profiler->addRaysTraced(rayBuffer->GetRayCount());
//
//}

//void CUDA_Worker::AdvanceEyePaths(RayBuffer *rayBuffer, EyePath* todoEyePaths,
//		uint* eyePathIndexes) {
//
//#ifndef __DEBUG
//	omp_set_num_threads(NUM_THREADS);
//#pragma omp parallel for schedule(guided)
//#endif
//	for (uint i = 0; i < rayBuffer->GetRayCount(); i++) {
//
//		EyePath *eyePath = &todoEyePaths[eyePathIndexes[i]];
//
//		const RayHit *rayHit = &rayBuffer->GetHitBuffer()[i];
//
//		if (rayHit->Miss()) {
//			// Add an hit point
//			//HitPointInfo &hp = *(engine->GetHitPointInfo(eyePath->pixelIndex));
//			//HitPointPositionInfo &hp = hitPointsStaticInfo_iterationCopy[eyePath->sampleIndex];
//			HitPointPositionInfo *hp = GetHitPointInfo(eyePath->sampleIndex);
//
//			//HitPoint &hp = GetHitPoint(hitPointsIndex++);
//			hp->type = CONSTANT_COLOR;
//			hp->scrX = eyePath->scrX;
//			hp->scrY = eyePath->scrY;
//
//			//						if (scene->infiniteLight)
//			//							hp.throughput = scene->infiniteLight->Le(
//			//									eyePath->ray.d) * eyePath->throughput;
//			//						else
//			//							hp.throughput = Spectrum();
//
//			if (ss->infiniteLight || ss->sunLight || ss->skyLight) {
//				//	hp.throughput = scene->infiniteLight->Le(eyePath->ray.d) * eyePath->throughput;
//
//				if (ss->infiniteLight)
//					ss->InfiniteLight_Le(&(hp->throughput),
//							(Vector*) &eyePath->ray.d, ss->infiniteLight,
//							ss->infiniteLightMap);
//				if (ss->sunLight)
//					ss->SunLight_Le(&hp->throughput, (Vector*) &eyePath->ray.d,
//							ss->sunLight);
//				if (ss->skyLight)
//					ss->SkyLight_Le(&hp->throughput, (Vector*) &eyePath->ray.d,
//							ss->skyLight);
//
//				hp->throughput *= eyePath->throughput;
//			} else
//				hp->throughput = Spectrum();
//
//			// Free the eye path
//			//ihp.accumPhotonCount = 0;
//			//ihp.accumReflectedFlux = Spectrum();
//			//ihp.photonCount = 0;
//			//hp.reflectedFlux = Spectrum();
//			eyePath->done = true;
//
//			//--todoEyePathCount;
//
//		} else {
//
//			// Something was hit
//			Point hitPoint;
//			Spectrum surfaceColor;
//			Normal N, shadeN;
//
//			if (engine->GetHitPointInformation(ss, &eyePath->ray, rayHit,
//					hitPoint, surfaceColor, N, shadeN))
//				continue;
//
//			// Get the material
//			const unsigned int currentTriangleIndex = rayHit->index;
//			const unsigned int currentMeshIndex =
//					ss->meshIDs[currentTriangleIndex];
//
//			const uint materialIndex = ss->meshMats[currentMeshIndex];
//
//			POINTERFREESCENE::Material *hitPointMat =
//					&ss->materials[materialIndex];
//
//			uint matType = hitPointMat->type;
//
//			if (matType == MAT_AREALIGHT) {
//
//				// Add an hit point
//				//HitPointInfo &hp = *(engine->GetHitPointInfo(
//				//		eyePath->pixelIndex));
//				//HitPointPositionInfo* hp = hitPointsStaticInfo_iterationCopy[eyePath->sampleIndex];
//
//				HitPointPositionInfo *hp = GetHitPointInfo(
//						eyePath->sampleIndex);
//
//				hp->type = CONSTANT_COLOR;
//				hp->scrX = eyePath->scrX;
//				hp->scrY = eyePath->scrY;
//				//ihp.accumPhotonCount = 0;
//				//ihp.accumReflectedFlux = Spectrum();
//				//ihp.photonCount = 0;
//				//hp.reflectedFlux = Spectrum();
//				Vector d = -eyePath->ray.d;
//				ss->AreaLight_Le(&hitPointMat->param.areaLight, &d, &N,
//						&hp->throughput);
//				hp->throughput *= eyePath->throughput;
//
//				// Free the eye path
//				eyePath->done = true;
//
//				//--todoEyePathCount;
//
//			} else {
//
//				Vector wo = -eyePath->ray.d;
//				float materialPdf;
//
//				Vector wi;
//				bool specularMaterial = true;
//				float u0 = getFloatRNG(seedBuffer[eyePath->sampleIndex]);
//				float u1 = getFloatRNG(seedBuffer[eyePath->sampleIndex]);
//				float u2 = getFloatRNG(seedBuffer[eyePath->sampleIndex]);
//				Spectrum f;
//
//				switch (matType) {
//
//				case MAT_MATTE:
//					ss->Matte_Sample_f(&hitPointMat->param.matte, &wo, &wi,
//							&materialPdf, &f, &shadeN, u0, u1,
//							&specularMaterial);
//					f *= surfaceColor;
//					break;
//
//				case MAT_MIRROR:
//					ss->Mirror_Sample_f(&hitPointMat->param.mirror, &wo, &wi,
//							&materialPdf, &f, &shadeN, &specularMaterial);
//					f *= surfaceColor;
//					break;
//
//				case MAT_GLASS:
//					ss->Glass_Sample_f(&hitPointMat->param.glass, &wo, &wi,
//							&materialPdf, &f, &N, &shadeN, u0,
//							&specularMaterial);
//					f *= surfaceColor;
//
//					break;
//
//				case MAT_MATTEMIRROR:
//					ss->MatteMirror_Sample_f(&hitPointMat->param.matteMirror,
//							&wo, &wi, &materialPdf, &f, &shadeN, u0, u1, u2,
//							&specularMaterial);
//					f *= surfaceColor;
//
//					break;
//
//				case MAT_METAL:
//					ss->Metal_Sample_f(&hitPointMat->param.metal, &wo, &wi,
//							&materialPdf, &f, &shadeN, u0, u1,
//							&specularMaterial);
//					f *= surfaceColor;
//
//					break;
//
//				case MAT_MATTEMETAL:
//					ss->MatteMetal_Sample_f(&hitPointMat->param.matteMetal, &wo,
//							&wi, &materialPdf, &f, &shadeN, u0, u1, u2,
//							&specularMaterial);
//					f *= surfaceColor;
//
//					break;
//
//				case MAT_ALLOY:
//					ss->Alloy_Sample_f(&hitPointMat->param.alloy, &wo, &wi,
//							&materialPdf, &f, &shadeN, u0, u1, u2,
//							&specularMaterial);
//					f *= surfaceColor;
//
//					break;
//
//				case MAT_ARCHGLASS:
//					ss->ArchGlass_Sample_f(&hitPointMat->param.archGlass, &wo,
//							&wi, &materialPdf, &f, &N, &shadeN, u0,
//							&specularMaterial);
//					f *= surfaceColor;
//
//					break;
//
//				case MAT_NULL:
//					wi = eyePath->ray.d;
//					specularMaterial = 1;
//					materialPdf = 1.f;
//
//					// I have also to restore the original throughput
//					//throughput = prevThroughput;
//					break;
//
//				default:
//					// Huston, we have a problem...
//					specularMaterial = 1;
//					materialPdf = 0.f;
//					break;
//
//				}
//
//				//						if (f.r != f2.r || f.g != f2.g || f.b != f2.b) {
//				//							printf("d");
//				//						}
//
//				if ((materialPdf <= 0.f) || f.Black()) {
//
//					// Add an hit point
//					//HitPointInfo &hp = *(engine->GetHitPointInfo(
//					//		eyePath->pixelIndex));
//					HitPointPositionInfo *hp = GetHitPointInfo(
//							eyePath->sampleIndex);
//					hp->type = CONSTANT_COLOR;
//					hp->scrX = eyePath->scrX;
//					hp->scrY = eyePath->scrY;
//					hp->throughput = Spectrum();
//					//ihp.accumPhotonCount = 0;
//					//ihp.accumReflectedFlux = Spectrum();
//					//ihp.photonCount = 0;
//					//hp.reflectedFlux = Spectrum();
//					// Free the eye path
//					eyePath->done = true;
//
//					//--todoEyePathCount;
//				} else if (specularMaterial || (!hitPointMat->difuse)) {
//
//					eyePath->throughput *= f / materialPdf;
//					eyePath->ray = Ray(hitPoint, wi);
//				} else {
//					// Add an hit point
//					//HitPointInfo &hp = *(engine->GetHitPointInfo(
//					//		eyePath->pixelIndex));
//					HitPointPositionInfo *hp = GetHitPointInfo(
//							eyePath->sampleIndex);
//					hp->type = SURFACE;
//					hp->scrX = eyePath->scrX;
//					hp->scrY = eyePath->scrY;
//					//hp.material
//					//		= (SurfaceMaterial *) triMat;
//					//ihp.accumPhotonCount = 0;
//					//ihp.accumReflectedFlux = Spectrum();
//					//ihp.photonCount = 0;
//					//hp.reflectedFlux = Spectrum();
//					hp->materialSS = materialIndex;
//
//					hp->throughput = eyePath->throughput * surfaceColor;
//					hp->position = hitPoint;
//					hp->wo = -eyePath->ray.d;
//					hp->normal = shadeN;
//
//					// Free the eye path
//					eyePath->done = true;
//
//					//--todoEyePathCount;
//				}
//
//			}
//
//		}
//	}
//
//}
