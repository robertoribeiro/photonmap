/*
 * job.cpp
 *
 *  Created on: Jul 25, 2012
 *      Author: rr
 */

#include "renderEngine.h"

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <FreeImage.h>
#include <boost/detail/container_fwd.hpp>
#include "luxrays/utils/sdl/scene.h"
#include "CUDA_Worker.h"
#include "CPU_Worker.h"
#include "config.h"
#include <boost/thread.hpp>
#include "cppbench.h"

using namespace boost;

CPPBENCH __p;
RenderConfig* cfg;

void ReadConfig(int argc, char *argv[]) {

	if (argc == 1) {

		cfg->width = 800;
		cfg->height = 600;
		cfg->superSampling = sqrt(1);
		cfg->photonsFirstIteration = (1 << 21);
		//cfg->photonsFirstIteration = (1 << 21) + (1 << 21); //1.5M;
		//cfg->photonsFirstIteration =2048*10; //1.5M;
		cfg->alpha = 0.7f;

		cfg->max_iterations = 500;

		//cfg->enginetype = PPM;
		//cfg->enginetype = SPPM;
		//cfg->enginetype = PPMPA;
		cfg->enginetype = SPPMPA;

		cfg->device_configuration = 601;

		std::string sceneFileName = "scenes/kitchen-simpler/kitchen.scn";
		//std::string sceneFileName = "scenes/bigmonkey-orange/bigmonkey.scn";
		//std::string sceneFileName = "scenes/psor-cube/psor-cube.scn";
		//	std::string sceneFileName = "scenes/luxball/luxball.scn";
		// std::string sceneFileName = "scenes/simple/simple.scn";



		//std::string sceneFileName = "scenes/kitchen/kitchen.scn";
		//std::string sceneFileName = "scenes/bowl/bowl.scn";
		//std::string sceneFileName = "scenes/diamond/diamond.scn";
		//  std::string sceneFileName = "scenes/classroom/classroom.scn";
		//	std::string sceneFileName = "scenes/cornell/cornell.scn";
		//	std::string sceneFileName = "scenes/simple-mat/simple-mat.scn";
		//	std::string sceneFileName = "scenes/sky/sky.scn";
		//	std::string sceneFileName = "scenes/studiotest/studiotest.scn";

		cfg->rebuildHash = 1;

		cfg->fileName = "image.png";

		cfg->sceneFileName = sceneFileName;

		cfg->GPU_AS = GPU_HASH_GRID;
		cfg->CPU_AS = PARALLEL_HASHGRID;
		cfg->sort_phits = 0;
		cfg->window = 0;

	} else if (argc == 16) {

		cfg->width = atoi(argv[1]);
		cfg->height = atoi(argv[2]);
		cfg->superSampling = sqrt(atoi(argv[3]));
		cfg->photonsFirstIteration = atoi(argv[4]);
		cfg->alpha = atof(argv[5]);
		cfg->enginetype = engineType(atoi(argv[6]));
		cfg->rebuildHash = atoi(argv[7]);
		cfg->max_iterations = atoi(argv[8]);
		cfg->GPU_AS = GPU_AS_TYPE(atoi(argv[9]));
		cfg->CPU_AS = CPU_AS_TYPE(atoi(argv[10]));
		cfg->device_configuration = atoi(argv[11]);
		cfg->sort_phits = atoi(argv[12]);
		cfg->sceneFileName = argv[13];
		cfg->fileName = argv[14];
		cfg->window = atoi(argv[15]);

	} else {

		fprintf(stderr, "Incorrect number of arguments.\n");
		exit(0);
	}

}

static void Draw(int argc, char *argv[]) {
	InitGlut(argc, argv, cfg->width, cfg->height);

	RunGlut(cfg->width, cfg->height);

}

inline void createWorker(devicesType d, Engine* engine) {

	Worker* w;

	switch (d) {
	case CPU0:
		w = new CPU_Worker(CPU0, engine);
		cfg->devices->insert(std::pair<devicesType, Worker*>(CPU0, w));
		cfg->ndevices++;
		break;
	case CPU1:
		w = new CPU_Worker(CPU1, engine);
		cfg->devices->insert(std::pair<devicesType, Worker*>(CPU1, w));
		cfg->ndevices++;
		break;
//	case CPU2:
//			w = new CPU_Worker(CPU2, engine);
//			cfg->devices->insert(std::pair<devicesType, Worker*>(CPU2, w));
//			cfg->ndevices++;
//			break;
//	case CPU3:
//			w = new CPU_Worker(CPU3, engine);
//			cfg->devices->insert(std::pair<devicesType, Worker*>(CPU3, w));
//			cfg->ndevices++;
//			break;
	case GPU0:
		w = new CUDA_Worker(GPU0, engine);
		cfg->devices->insert(std::pair<devicesType, Worker*>(GPU0, w));
		cfg->ndevices++;
		break;
	case GPU1:
		w = new CUDA_Worker(GPU1, engine);
		cfg->devices->insert(std::pair<devicesType, Worker*>(GPU1, w));
		cfg->ndevices++;
		break;
	case GPU2:
		w = new CUDA_Worker(GPU2, engine);
		cfg->devices->insert(std::pair<devicesType, Worker*>(GPU2, w));
		cfg->ndevices++;
		break;
	default:
		printf("Device initizalizaiton code not specified\n");
		assert(false);
		break;
	}

}

inline void createWorkers() {

	Engine* engine = cfg->GetEngine();
	uint dev_config = cfg->device_configuration;

	switch (dev_config) {
	/*  PERFORM */
//	case 200:
//		createWorker(CPU0, engine);
//		fprintf(stderr, "Using CPU0\n");
//		break;
//	case 201:
//		createWorker(GPU0, engine);
//		fprintf(stderr, "Using GPU0\n");
//		break;
//	case 202:
//		createWorker(CPU0, engine);
//		createWorker(GPU0, engine);
//		fprintf(stderr, "Using CPU0 + GPU0\n");
//		break;
//	case 203:
//		createWorker(GPU0, engine);
//		createWorker(GPU2, engine);
//		fprintf(stderr, "Using GPU0 + GPU2\n");
//		break;
//	case 204:
//		createWorker(CPU0, engine);
//		createWorker(GPU0, engine);
//		createWorker(GPU2, engine);
//		fprintf(stderr, "Using CPU0 + GPU0 + GPU2\n");
//		break;
	/* THOR */
	case 610:
		createWorker(CPU0, engine);
		fprintf(stderr, "Device configuration|CPU0\n");
		break;
	case 620:
		createWorker(CPU0, engine);
		createWorker(CPU1, engine);
		fprintf(stderr, "Device configuration|CPU0 + CPU1\n");
		break;
	case 601:
		createWorker(GPU0, engine);
		fprintf(stderr, "Device configuration|GPU0\n");
		break;
	case 6012:
		createWorker(GPU1, engine);
		fprintf(stderr, "Device configuration|GPU1\n");
		break;
	case 611:
		createWorker(CPU0, engine);
		createWorker(GPU0, engine);
		fprintf(stderr, "Device configuration|CPU0 + GPU0\n");
		break;
	case 621:
		createWorker(CPU0, engine);
		createWorker(CPU1, engine);
		createWorker(GPU0, engine);
		fprintf(stderr, "Device configuration|CPU0 + CPU1 + GPU0\n");
		break;
	case 602:
		createWorker(GPU0, engine);
		createWorker(GPU1, engine);
		fprintf(stderr, "Using GPU0 + GPU1\n");
		break;
	case 612:
		createWorker(CPU0, engine);
		createWorker(GPU0, engine);
		createWorker(GPU1, engine);
		fprintf(stderr, "Device configuration|CPU0 + GPU0 + GPU1\n");
		break;
	case 622:
		createWorker(CPU0, engine);
		createWorker(CPU1, engine);
		createWorker(GPU0, engine);
		createWorker(GPU1, engine);
		fprintf(stderr, "Using CPU0 + CPU1+ GPU0 + GPU1\n");
		break;
	case 603:
		createWorker(GPU0, engine);
		createWorker(GPU1, engine);
		createWorker(GPU2, engine);
		fprintf(stderr, "Device configuration|GPU0 + GPU1 + GPU2\n");
		break;
	case 613:
		createWorker(CPU0, engine);
		createWorker(GPU0, engine);
		createWorker(GPU1, engine);
		createWorker(GPU2, engine);
		fprintf(stderr, "Device configuration|CPU0 + GPU0 + GPU1 + GPU2\n");
		break;
	case 623:
		createWorker(CPU0, engine);
		createWorker(CPU1, engine);
		createWorker(GPU0, engine);
		createWorker(GPU1, engine);
		createWorker(GPU2, engine);
		fprintf(stderr,
				"Device configuration|CPU0 + CPU1 + GPU0 + GPU1 + GPU2\n");
		break;
	default:
		break;
	}
}

int main(int argc, char *argv[]) {

	__p.reg("Walltime");

	srand(1000);

//	printf(" EyePath %lu\n", sizeof(EyePath));
//	printf(" HitPoint %lu\n", sizeof(HitPoint));
//	printf("HitPointPositionInfo %lu\n", sizeof(HitPointPositionInfo));
//	printf("HitPointRadianceFlux %lu\n", sizeof(HitPointRadianceFlux));
//	printf("PhotonHit %lu\n", sizeof(PhotonHit));
//	printf("Ray %lu\n", sizeof(Ray));

	cfg = new RenderConfig();

	ReadConfig(argc, argv);

	if (cfg->GetEngineType() == SPPM || cfg->GetEngineType() == SPPMPA)
		cfg->superSampling = 1;

	cfg->SetHitPointTotal(
			cfg->width * cfg->height * cfg->superSampling * cfg->superSampling);

	Engine* engine;
	switch (cfg->enginetype) {
	case PPM:
		engine = new PPMEngine();
		fprintf(stderr, "Engine|PPM\n");
		break;
	case SPPM:
		engine = new SPPMEngine();
		fprintf(stderr, "Engine|SPPM\n");
		break;
	case PPMPA:
		engine = new PPMPAEngine();
		fprintf(stderr, "Engine|PPMPA\n");
		break;
	case SPPMPA:
		engine = new SPPMPAEngine();
		fprintf(stderr, "Engine|SPPMPA\n");
		break;
	default:
		break;
	}

	cfg->engine = engine;

	engine->ss = new PointerFreeScene(cfg->width, cfg->height,
			cfg->sceneFileName);

	createWorkers();

	if (cfg->GetEngineType() == PPM || cfg->GetEngineType() == SPPM)
		assert(cfg->ndevices == 1);

#ifdef ENABLE_TIME_BREAKDOWN
	assert(cfg->ndevices == 1);
#endif

//#ifdef __DEBUG
//	assert(cfg->device_configuration <= 2);
//#endif

	//Only one device builds the hitpoints
	if (cfg->GetEngineType() == PPMPA)
		((PPMPAEngine*) engine)->waitForHitPoints = new boost::barrier(
				cfg->ndevices);

	__p.reg("Total Job");

	cfg->startTime = WallClockTime();

	bool build_hit = true;
	std::map<devicesType, Worker*>::iterator devs;

	for (devs = cfg->devices->begin(); devs != cfg->devices->end(); devs++) {
		devs->second->Start(build_hit);
		build_hit = false;
	}

	if (cfg->window)
		engine->draw_thread = new boost::thread(Draw, argc, argv);

	for (devs = cfg->devices->begin(); devs != cfg->devices->end(); devs++) {
		devs->second->thread->join();
	}

	engine->splatWorkerKill();

	engine->splatWorker->join();

	if (engine->work->empty())
		printf("All sampleBuffer processed!\n");
	else
		printf("There are ampleBuffers not processed!\n");

	__p.stp("Total Job");
	const double elapsedTime = WallClockTime() - cfg->startTime;

	float MPhotonsSec = engine->getPhotonTracedTotal()
			/ (elapsedTime * 1000000.f);

	const float itsec = engine->GetIterationNumber() / elapsedTime;

	printf("\nSystem|execution time|%.3f\n", elapsedTime);
	printf("System|MPhotons/sec|%.3f\n", MPhotonsSec);
	printf("System|iteration/sec|%.3f\n", itsec);
	printf("Total photons: %.2fM\n",
			engine->getPhotonTracedTotal() / 1000000.f);

	//printf("Splat flux call count: %d\n", cpuWorker->lookupA->call_times);

	__p.stp("Walltime");

#ifdef ENABLE_TIME_BREAKDOWN
	__p.PRINTALL_SECONDS();
#endif

	if (cfg->window)
		engine->draw_thread->join();

	printf("GRACEFUL_EXIT\n");

	return EXIT_SUCCESS;
}
