iterations/sec,1.54844
PM MPhotons/sec,6.56109
PM MRays/sec,30.4414
PT MEyeSamples/sec,5.75485
PT MRays/sec,10.2019
System execution time,324.563
System iteration/sec,1.547
System MPhotons/sec,3.231
Walltime                                                                    , 324.722 
Total Job                                                                   , 324.563 
Process Iterations                                                          , 323.203 
Build Hit Points                                                            , 0.083 
Update BBox                                                                 , 0.005 
Process Iterations > Iterations                                             , 322.905 
Process Iterations > Initialize radius                                      , 0.002 
Process Iterations > Iterations > Update lookup                             , 0.091 
Process Iterations > Iterations > Build Photon Map                          , 312.796 
Process Iterations > Iterations > Build Photon Map > Trace                  , 159.815 
Process Iterations > Iterations > Build Photon Map > Search                 , 152.975 
Process Iterations > Iterations > Radiance calc                             , 2.251 
Process Iterations > Iterations > Update Samples                            , 7.750 
Process Iterations > Iterations > Update Samples > HP to sample             , 0.963 
Process Iterations > Iterations > Update Samples > Copy samples RGB         , 2.242 
Process Iterations > Iterations > Update Samples > Splat to pixel           , 3.897 
