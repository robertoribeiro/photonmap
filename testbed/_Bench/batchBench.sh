#!/bin/bash
#set -x


if [ $# -ne 2 ] 
then
echo "usage: [casePath] [profile|performance]"
exit;
else

_casePath=$1
_case=`basename $1`
_appliance=$2

#		[scene] [engine] [devices] [CPU_AS] [GPU_AS] [spp] [res] [photons] [max_its]"

#_query=( kitchen SPPM all cas_PARALLEL_HASHGRID gas_GPU_HASHGRID 1 800x600 2097152 500 )
#_query=( kitchen SPPMPA all cas_PARALLEL_HASHGRID gas_GPU_HASHGRID 1 800x600 2097152 500 )
#_query=( kitchen PPMPA all cas_PARALLEL_HASHGRID gas_GPU_HASHGRID 1 800x600 2097152 500 )
#_query=( kitchen PPM all cas_PARALLEL_HASHGRID gas_GPU_HASHGRID 1 800x600 2097152 500 )

_query=( kitchen SPPMPA 601 cas_PARALLEL_HASHGRID gas_GPU_HASHGRID 1 800x600 2097152 500 )

_queryString=`echo ${_query[@]} | sed 's/ /-/g'`

if [ ${#_query[@]} -ne 9 ] 
then
echo "Wrong query length"
exit;
fi

./sample.sh $_casePath ${_query[@]}
_error=`echo $?`

if [[ $_error -eq 0 ]]; then
	./parse.sh $_case $_queryString $_appliance
fi

fi
