#!/bin/bash
set -x



if [ $# -ne 10 ] 
then
echo "usage: sample [folder] [scene] [engine] [devices] [CPU_AS] [GPU_AS] [spp] [res] [photons] [max_its]"
exit;
else


folder=`basename $1`
scene=$2
engine=$3
devices=$4
CPU_AS=$5
GPU_AS=$6
spp=$7
res=$8
photons=$9
max_its=${10}

results_file=results.txt

work_folder="/home/rr/work/nsight-photomap/photonmap/testbed/_Bench"

mkdir $work_folder"/"$folder

sampleString=$scene"-"$engine"-"$devices"-"$CPU_AS"-"$GPU_AS"-"$spp"-"$res"-"$photons"-"$max_its

gathering_folder=$work_folder"/"$folder"/"$sampleString

mkdir $gathering_folder

function go_in {
	if [[ ! -e $1 ]]; then
		echo "Error: parameter $1 folder does not exist!"
		pwd
		exit 1;
	else
		if [ -d $1 ]; then
			cd $1
		else
			echo "Error: $1 is not a folder!"
			pwd
			exit 1;
		fi
	fi
}

function copy_result {
	set -x
	cp $results_file $gathering_folder"/"$1
	set +x
}


#if the parameter has th prefix all_, iterates over all folders and copy the results file prefixed with the parameter value
#Only one all_ is allowed
function depth {

	local currentArg=$1

	if [ $currentArg -eq 10 ]; then 
		copy_result $iterative_p
		return;
	fi

	if [[ ${args[$currentArg]} == "all"* ]]; then 
		for D in *; do
			iterative_p=$D #saves current iterative parameter
			old=`pwd`
			go_in $D
			depth $((currentArg + 1))
			cd $old
		done
	else
		go_in ${args[$currentArg]}
		depth $((currentArg + 1))
	fi
}



cd $1"/output"
currentArg=1
args=("$@")
depth $currentArg


fi



