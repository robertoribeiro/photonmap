iterations/sec,1.02899
PM MPhotons/sec,2.56727
PM MRays/sec,7.06563
PT MEyeSamples/sec,19.6311
PT MRays/sec,21.3183
System execution time,487.535
System iteration/sec,1.030
System MPhotons/sec,2.151
Walltime                                                                    , 487.854 
Total Job                                                                   , 487.535 
Process Iterations                                                          , 486.133 
Process Iterations > Iterations                                             , 485.915 
Process Iterations > Iterations > Build Hit Points                          , 12.227 
Process Iterations > Iterations > Update BBox                               , 2.401 
Process Iterations > Iterations > Initialize radius                         , 0.001 
Process Iterations > Iterations > Update lookup                             , 4.293 
Process Iterations > Iterations > Build Photon Map                          , 443.177 
Process Iterations > Iterations > Build Photon Map > Trace                  , 382.232 
Process Iterations > Iterations > Build Photon Map > Sort pho hits          , 26.200 
Process Iterations > Iterations > Build Photon Map > Sort pho hits > bb     , 4.483 
Process Iterations > Iterations > Build Photon Map > Sort pho hits > sort   , 3.361 
Process Iterations > Iterations > Build Photon Map > Sort pho hits > move   , 17.266 
Process Iterations > Iterations > Build Photon Map > Search                 , 34.724 
Process Iterations > Iterations > Radiance calc                             , 2.140 
Process Iterations > Iterations > Update Samples                            , 21.634 
Process Iterations > Iterations > Update Samples > HP to sample             , 0.963 
Process Iterations > Iterations > Update Samples > Copy samples RGB         , 1.843 
Process Iterations > Iterations > Update Samples > Splat to pixel           , 18.256 
