#!/bin/bash
#set -x
IFS=$'\n'

cd $1"/"$2

final=profile/"final.xls"

mkdir profile

#benchmark table uses Device X-Metric,value ; profile does not
function query_value {
	case  $1 in 
		1) query_value1 $2 $3
			;;
		2) 	query_value2 $2 $3
			;;
		3) 	query_value3 $2 $3
		;;
		
	esac
}

function query_value1 {
	a="/$1/{print \$2, \$3}"
	awk -F'|' -v OFS=',' $a $2
}

function query_value2 {
	a="/$1/{print \$1 \" \" \$2, \$3}"
	awk -F'|' -v OFS=',' $a $2
}

function query_value3 {
	a="/$1/{print \$1 \";\" \$2, \$3}"
	awk -F'|' -v OFS=',' $a $2
}


if [[ $3 == profile ]]; then
method1=1
method2=2
all_string=""
else 
method1=3
method2=3
all_string=","
fi

counter=2
runs=`find . -maxdepth 1 -type f -print | cut -d'/' -f2`

if [ ${#runs[@]} -eq 0 ] 
then
echo "Error: query provided no results!"
exit 1;
fi

for run in $runs; do
	
	all_string=$all_string",$run"
	echo $all_string
	
	gather_file="profile/profile-"$run

	grep -e 'Device [0-9]*|' $run > aux

	query_value $method1 "PM MPhotons\/sec" aux >> $gather_file	
	query_value $method1 "PM MRays\/sec" aux >> $gather_file	
	query_value $method1 "PT MEyeSamples\/sec" aux >> $gather_file	
	query_value $method1 "PT MRays\/sec" aux >> $gather_file	
	query_value $method1 "iterations\/sec" aux >> $gather_file	
	rm aux
	
	query_value $method2 "System\|execution time" $run >> $gather_file	
	query_value $method2 "System\|MPhotons\/sec" $run  >> $gather_file	
	query_value $method2 "System\|iteration\/sec" $run >> $gather_file	

	sort -t- -k1 -k2 $gather_file > tmp2
	rm $gather_file
	cat tmp2 >> $gather_file
	rm tmp2

	if [[ $3 == profile ]]; then
		awk -F'|' -v OFS=',' '/^Walltime/{l++} l==1{ if (match($1, /=+/)) exit; print $1, $2}' $run >> $gather_file	
	fi

	if [[ ! -e $final ]]; then
		cp $gather_file $final
	else
		order=$order",1."$counter
		counter=$((counter +1))
		join -t, -a1 -a2 -e "NA" -o 0$order,2.2 $final $gather_file >> tmp
		mv tmp $final
	fi

done

#dont want to sort profile table
if [[ $3 == profile ]]; then
	( ( head -n 8 $final | sort -r -t- -k1 -k2 ) && tail -n +9 $final ) > tmp2
	rm $final
	echo $all_string > $final	
	cat tmp2 >> $final
	rm tmp2
else
	sort -r -t- -k1 -k2 $final > tmp2
	rm $final
	echo $all_string > $final	
	cat tmp2 >> $final
	rm tmp2
fi
