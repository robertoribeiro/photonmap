

testname="profile-withfilternoasync"

############# partial base set ############

widthxheight=( "800 600" )
superSampling=( 1 )
photonsFirstIteration=( $((1024*2048)) )
max_iterations=( 500 )
device_configuration=( 610 601 ) 
sceneFileName=("scenes/kitchen-simpler/kitchen.scn" )
engine=( 3 2 )
GPU_AS=( 0 )
CPU_AS=( 2 )

################## secondary #####################

fileName="image.tiff"
alpha=0.7
rebuildHash=1
sort_phits=1
