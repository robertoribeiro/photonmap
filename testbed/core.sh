#!/bin/bash
set -x


if [ $# -gt 0 ] 
then

######################################

. $1

######################################

unique=`date "+%s%N"`

work_folder="/home/rr/work/nsight-photomap"
results_folder=$work_folder"/photonmap/testbed"
out_folder=$results_folder"/"$testname$unique

if [[ ! -e $out_folder ]]
then
mkdir $out_folder


cp $1 $out_folder

out_folder=$out_folder"/output"
mkdir $out_folder

out=$out_folder/results.txt


BIN1="$work_folder/photonmap/Release/photonmap"
create_window=0

engineNames=(PPM SPPM PPMPA SPPMPA)
GPU_ASnames=(GPU_HASHGRID GPU_MORTON_HASHGRID GPU_MORTONGRID)
CPU_ASnames=( KDTREE HASHGRID PARALLEL_HASHGRID )

################ full base set ##########

#widthxheight=("1024 768" "800 600")
#superSampling=(1 4 9 16 25)
#photonsFirstIteration=($((1024*512)) $((1024*1024)) $((1024*2048)) $((1024*4096)) $((1024*8192)))
#max_iterations=( 1000 )
#device_configuration=(0 1 2 3 4)
#sceneFileName=("scenes/kitchen/kitchen.scn" "scenes/kitchen-cube/kitchen.scn" "scenes/bigmonkey-orange/bigmonkey.scn" "scenes/diamond/diamond.scn" "scenes/psor-cube/psor-cube.scn" "scenes/classroom/classroom.scn" "scenes/luxball/luxball.scn" "scenes/cornell/cornell.scn" "scenes/simple/simple.scn" "scenes/simple-mat/simple-mat.scn" "scenes/studiotest/studiotest.scn" "scenes/psor-cube2/psor-cube2.scn")
#fileName="image.tiff"
#engine=(0 1 2 3)
#GPU_AS=( 0 1 2 )
#CPU_AS=( 0 1 2 ) 




function singleDevice {

			single=0

		 	if [[ $1 -eq 610 ]] ; then	
		 		single=1
		 	fi

		 	if [[ $1 -eq 601 ]] ; then	
		 		single=1
		 	fi


		 	echo "$single"
}

itc=0
it=0
cd $out_folder"/"
function doLoops {

	if  [[ $1 == 0 ]]; then
		set +x
	fi

	for i_sceneFileName in "${sceneFileName[@]}"; do
		
		sedi_sceneFileName=`echo $i_sceneFileName | sed 's/scenes\/.*\///' | sed 's/\.scn//'`

		if  [[ $1 == 1 ]]; then
		mkdir $sedi_sceneFileName
		cd $sedi_sceneFileName
		fi
		for i_engine in "${engine[@]}" ; do
			
			if  [[ $1 == 1 ]]; then
			mkdir ${engineNames[$i_engine]}
			cd ${engineNames[$i_engine]}
			fi

			for i_device_configuration in "${device_configuration[@]}"; do

				if [[ $i_engine == 0 ]] || [[ $i_engine == 1 ]]; then
					single=$(singleDevice $i_device_configuration)
				 	if [[ $single -ne 1 ]] ; then	
						continue;
					fi 
				fi
				
				if  [[ $1 == 1 ]]; then
				mkdir $i_device_configuration
				cd $i_device_configuration
				fi

				for i_CPU_AS in "${CPU_AS[@]}" ; do

					if  [[ $1 == 1 ]]; then
					mkdir "cas_"${CPU_ASnames[$i_CPU_AS]}
					cd "cas_"${CPU_ASnames[$i_CPU_AS]}
					fi

					for i_GPU_AS in "${GPU_AS[@]}"; do

						if  [[ $1 == 1 ]]; then
						mkdir "gas_"${GPU_ASnames[$i_GPU_AS]}
						cd "gas_"${GPU_ASnames[$i_GPU_AS]}
						fi

						for i_superSampling in "${superSampling[@]}" ; do

							if  [[ $1 == 1 ]]; then
								mkdir $i_superSampling
								cd $i_superSampling
							fi

							for i_widthxheight in "${widthxheight[@]}"; do

								sedi_widthxheight=`echo $i_widthxheight | tr " " x`

								if  [[ $1 == 1 ]]; then
								mkdir $sedi_widthxheight
								cd $sedi_widthxheight
								fi

								for i_photonsFirstIteration in "${photonsFirstIteration[@]}"; do

									if  [[ $1 == 1 ]]; then
									mkdir $i_photonsFirstIteration
									cd $i_photonsFirstIteration
									fi

									for i_max_iterations in "${max_iterations[@]}" ; do
										
										#executionString=$sedi_widthxheight"x"$i_superSampling"x"$i_max_iterations"-ph_"$i_photonsFirstIteration"-dev_"$i_device_configuration"-scn_"$sedi_sceneFileName

										if  [[ $1 == 1 ]]; then
										mkdir $i_max_iterations
										cd $i_max_iterations
										fi

										RUN=0
										execTimes=""
										while [ $RUN -lt 2 ]; do

											if  [[ $1 == 0 ]]; then
												it=$((it+1))
											else 


												if [[ -e "RUN"$RUN ]]; then
													pwd
													exit;
												fi

												mkdir "RUN"$RUN
												cd "RUN"$RUN

												itc=$((itc+1))
												echo "EXECUTION: "$itc"/"$it
										
												cp -d $out_folder"/../../scenes" .

												params="$i_widthxheight $i_superSampling $i_photonsFirstIteration $alpha $i_engine $rebuildHash $i_max_iterations $i_GPU_AS $i_CPU_AS $i_device_configuration $sort_phits $i_sceneFileName $fileName $create_window"
												echo $params > results.txt

												COUNTER=0
												grace=""
												while [ $COUNTER -lt 3 ] && [[ $grace != "GRACEFUL_EXIT" ]]; do
													$BIN1 $i_widthxheight $i_superSampling $i_photonsFirstIteration $alpha $i_engine $rebuildHash $i_max_iterations $i_GPU_AS $i_CPU_AS $i_device_configuration $sort_phits $i_sceneFileName $fileName $create_window 2>&1 | tee -a results.txt
													grace=$(grep -Fx "GRACEFUL_EXIT" results.txt)
													let COUNTER=COUNTER+1 

													if [[ $grace != "GRACEFUL_EXIT" ]]; then
														echo $params &>> $out_folder"/"fail.txt
														rm results.txt
										            fi
												done

												if [ $RUN -eq 0 ]; then 
													execTimes=$(grep "System|execution time|" results.txt | sed 's/System|execution time|//g'| bc)
												else
													execTimes=$execTimes"\n"$(grep "System|execution time|" results.txt | sed 's/System|execution time|//g'| bc)
												fi
												rm scenes										
												cd ..
											fi
											let RUN=RUN+1 
									    done				
										
										if  [[ $1 == 1 ]]; then
											echo $execTimes
											minRun=$( echo -e $execTimes | awk 'NR == 1 { max=$1; min=$1; total=0; a=0; count=0;}  { if ($1>max) max=$1; if ($1<min) {min=$1; a=count;}  total+=$1; count+=1}   END {printf "%d",  a}' )
											echo $minRun
											cp "RUN"$minRun/* .
										fi

										#if  [[ $1 == 1 ]]; then
											#exit;
										#fi

										if  [[ $1 == 1 ]]; then
										cd ..
										fi
									done

									if  [[ $1 == 1 ]]; then
									cd ..
									fi
								done

								if  [[ $1 == 1 ]]; then
								cd ..
								fi
							done

							if  [[ $1 == 1 ]]; then
								cd ..
							fi

							if [[ $i_engine == 1 ]] || [[ $i_engine == 3 ]]; then
								break;
							fi
						done
						if  [[ $1 == 1 ]]; then
						cd ..
						fi
						if [[ $i_device_configuration == 610 ]] || [[ $i_device_configuration == 620 ]]; then
							break;
						fi
					done
					if  [[ $1 == 1 ]]; then
					cd ..
					fi
					if [[ $i_device_configuration == 601 ]] || [[ $i_device_configuration == 602 ]] || [[ $i_device_configuration == 603 ]]; then
				 			break;
					fi
				done
				if  [[ $1 == 1 ]]; then
				cd ..
				fi
			done
			if  [[ $1 == 1 ]]; then
			cd ..
			fi
		done
		if  [[ $1 == 1 ]]; then
		cd ..
		fi
	done	

	if  [[ $1 == 0 ]]; then
		set -x
	fi
}

doLoops 0
echo $it
`pwd`
doLoops 1

else
echo "Nothing done!! Folder exists, add a different comment"
fi

else 
echo "Nothing done!! Add a comment to the folder"
fi
