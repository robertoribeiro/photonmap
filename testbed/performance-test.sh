
#testname="performance_SPPMPA_"
testname="performance-620-602"

############# partial base set ############

widthxheight=( "800 600" )
superSampling=( 1 )
photonsFirstIteration=( $((1024*2048)) )
max_iterations=( 500 ) 
#device_configuration=( 622 612 621 611 620 602 )
device_configuration=( 620 602 )
sceneFileName=("scenes/kitchen-simpler/kitchen.scn" "scenes/bigmonkey-orange/bigmonkey.scn" "scenes/psor-cube/psor-cube.scn" "scenes/simple/simple.scn" "scenes/luxball/luxball.scn" )
engine=( 3 2 )
GPU_AS=( 0 )
CPU_AS=( 2 )

################## secondary #####################

fileName="image.tiff"
alpha=0.7
rebuildHash=1
sort_phits=1
