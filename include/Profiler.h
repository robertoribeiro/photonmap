/*
 * Profiler.h
 *
 *  Created on: Nov 9, 2012
 *      Author: rr
 */

#ifndef PROFILER_H_
#define PROFILER_H_

#include <stdio.h>
#include <iostream>

class Profiler {
public:

	unsigned long long photonsTraced;
	unsigned long long photonRaysTraced;

	unsigned long long EyesamplesTraced;
	unsigned long long EyesamplesRaysTraced;

	unsigned int iterationCount;

	double photonTracingTime;
	double rayTracingTime;
	double iteratingTime;

	Profiler() {
		photonsTraced=0;
		photonRaysTraced=0;
		EyesamplesTraced=0;
		EyesamplesRaysTraced=0;

		iterationCount=0;

		photonTracingTime = 0.0f;
		rayTracingTime = 0.0f;
		iteratingTime = 0.0f;
	}
	virtual ~Profiler() {

	}

	void addPhotonsTraced(unsigned long long int p) {
		photonsTraced += p;
	}

	void addphotonRaysTraced(unsigned long long int p) {
		photonRaysTraced += p;
	}

	void addEyesamplesTraced(unsigned long long p) {
		EyesamplesTraced += p;
	}

	void addEyesamplesRaysTraced(unsigned long long p) {
		EyesamplesRaysTraced += p;
	}

	void addIteration(unsigned int p) {
		iterationCount += p;
	}

	void addPhotonTracingTime(double p) {
		photonTracingTime += p;
	}

	void addRayTracingTime(double p) {
		rayTracingTime += p;
	}

	void additeratingTime(double p) {
		iteratingTime += p;
	}

	void printStats(uint id) {

		stringstream s;

		s << "\n\n";

		double MPhotonsSec = double(photonsTraced) / (photonTracingTime * 1000000.f);
		double MEyeSamplesSec = (EyesamplesTraced == 0) ? 0 : (double(EyesamplesTraced) / (rayTracingTime * 1000000.f));


		double itsec = double(iterationCount) / iteratingTime;
		double PTMRaysSec = (EyesamplesRaysTraced == 0) ? 0 : (double(EyesamplesRaysTraced) / (rayTracingTime * 1000000.f));
		double PMMRaysSec = double(photonRaysTraced) / (photonTracingTime * 1000000.f);

		s << "Device " << id << "|PM MPhotons/sec|" << MPhotonsSec << "\n";
		s << "Device " << id << "|PM MRays/sec|" << PMMRaysSec << "\n";
		s << "Device " << id << "|PT MEyeSamples/sec|" << MEyeSamplesSec << "\n";
		s << "Device " << id << "|PT MRays/sec|" << PTMRaysSec << "\n";
		s << "Device " << id << "|iterations/sec|" << itsec << "\n";

		cout << s.str();

	}

};

#endif /* PROFILER_H_ */
