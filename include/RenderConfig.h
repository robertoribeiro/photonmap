/*
 * RenderConfig.h
 *
 *  Created on: May 9, 2013
 *      Author: rr
 */

#ifndef RENDERCONFIG_H_
#define RENDERCONFIG_H_

#include "device_ordinals.h"
#include <hwloc.h>

class Engine;
class RenderConfig;
class Worker;

extern RenderConfig* cfg;

typedef enum engine_t {
	PPM = 0, SPPM = 1, PPMPA = 2, SPPMPA = 3
} engineType;

typedef enum CPU_AS_TYPE_t {
	KDTREE = 0, HASHGRID = 1, PARALLEL_HASHGRID = 2,
} CPU_AS_TYPE;

typedef enum GPU_AS_TYPE_t {
	GPU_HASH_GRID = 0, GPU_MORTON_HASH_GRID = 1, GPU_MORTON_GRID = 2,
} GPU_AS_TYPE;

class RenderConfig {
public:
	RenderConfig() {

		devices = new std::map<devicesType, Worker*>();

		scrRefreshInterval = 1000;
		startTime = 0.0;

		SPPMG_LABEL = (char*) "Many-core Progressive Photon Mapping";

		ndevices = 0;

		hitPointTotal = 0;

		hwloc_topology_init(&topology);
		hwloc_topology_load(topology);

		support = hwloc_topology_get_support(topology);

		root = hwloc_get_root_obj(topology);

	}
	virtual ~RenderConfig() {

	}

	Engine* GetEngine() {
		return engine;
	}

	engineType GetEngineType() {
		return enginetype;
	}

	std::map<devicesType, Worker*>* devices;

	uint device_configuration;
	uint ndevices;
	engineType enginetype;

	Engine* engine;

	unsigned int photonsFirstIteration;
	unsigned int width;
	unsigned int height;
	unsigned int superSampling;
	float alpha;
	char* SPPMG_LABEL;

	double startTime;
	unsigned int scrRefreshInterval;
	std::string fileName;
	bool rebuildHash;

	uint max_iterations;

	std::string sceneFileName;

	GPU_AS_TYPE GPU_AS;
	CPU_AS_TYPE CPU_AS;

	bool sort_phits;
	uint photons_per_slice;
	uint PHOTON_HIT_BUFFER_SIZE;

	hwloc_topology_t topology;
	const struct hwloc_topology_support *support;

	hwloc_obj_t root;

	bool window;

private:
	unsigned int hitPointTotal;

public:
	unsigned int inline GetHitPointTotal() {
		return hitPointTotal;
	}

	void inline SetHitPointTotal(unsigned int a) {
		hitPointTotal = a;
	}

};

#endif /* RENDERCONFIG_H_ */
