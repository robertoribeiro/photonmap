/*
 * cbench.h
 *
 *  Created on: Jul 17, 2012
 *      Author: rr
 */

#ifndef CPPBENCH_H_
#define CPPBENCH_H_

#include <string>
#include <iostream>
#include <map>
#include <sys/time.h>

//#define ENABLE_TIME_BREAKDOWN

typedef unsigned int uint;

#define ENABLE

#define MAX_LEVELS 10

using namespace std;

inline double CPPBENCHWallClockTime() {
#if defined(__linux__) || defined(__APPLE__) || defined(__CYGWIN__)
	struct timeval t;
	gettimeofday(&t, NULL);

	return t.tv_sec + t.tv_usec / 1000000.0;
#elif defined (WIN32)
	return GetTickCount() / 1000.0;
#else
#error "Unsupported Platform !!!"
#endif
}

class CPPBENCH;

extern CPPBENCH __p;

class CPPBENCH {
public:

	map<string, double>* timers;
	map<string, uint>* insertion_order;

	uint lvl;

	CPPBENCH() {
#ifdef ENABLE_TIME_BREAKDOWN
		timers = new map<string, double>();
		insertion_order = new map<string, uint>();
		lvl = 0;
#endif
	}

	~CPPBENCH() {
	}

	void inline reg(string s, bool NA=false) { //REGISTER
#ifdef ENABLE_TIME_BREAKDOWN
		insertion_order->insert(pair<string, uint>(s,lvl++));
		timers->insert(pair<string, double>(s, NA ? -1 : CPPBENCHWallClockTime()));
#endif

	}

	void inline stp(string s, bool NA=false) { //STOP
#ifdef ENABLE_TIME_BREAKDOWN
		(*timers)[s] = NA ? -1 : (CPPBENCHWallClockTime() - (*timers)[s]);
#endif

	}

	void PRINT_SECONDS(string s) {
#ifdef ENABLE_TIME_BREAKDOWN
		printf("CBENCH |  %s: %.2f2s\n", s.c_str(), (*timers)[s]);
#endif

	}

	double GET_SECONDS(string s) {
#ifdef ENABLE_TIME_BREAKDOWN
		return ((*timers)[s]);
#endif
	}

	/*
	 * Prev + (end - star) = (Prev - start) + end
	 * And a start is always after nothing or a stop
	 */
	void inline lsstt(string s, bool NA=false) { //LOOP_STAGE_START
#ifdef ENABLE_TIME_BREAKDOWN
		map<string, double>::iterator it;

		it = timers->find(s);

		if (it != timers->end()) {
			(*timers)[string(s)] -= NA ? -1 : CPPBENCHWallClockTime();
		} else {
			insertion_order->insert(pair<string, uint>(s,lvl++));
			timers->insert(pair<string, double>(s, NA ? -1 : -CPPBENCHWallClockTime()));
		}
#endif
	}

	void inline lsstp(string s, bool NA=false) { //LOOP_STAGE_STOP
#ifdef ENABLE_TIME_BREAKDOWN

		(*timers)[s] += NA ? -1 : CPPBENCHWallClockTime();
#endif

	}

	void PRINTALL_SECONDS() {
#ifdef ENABLE_TIME_BREAKDOWN

		string* ordered = new string[timers->size()];
		map<string, double>::iterator it;

		for (it = timers->begin(); it != timers->end(); it++)
			ordered[insertion_order->at(it->first)] = it->first;

		printf(
				"\n\n=====================================================================================\n");

		for (uint i = 0; i < timers->size(); i++)

			if (timers->at(ordered[i]) < 0){
				printf("%-75.75s | %-2.3s |\n", ordered[i].c_str(),
									"NA");
			}
			else
			printf("%-75.75s | %-2.3f |\n", ordered[i].c_str(),
					timers->at(ordered[i]));
		printf(
				"=====================================================================================\n");
#endif

	}

//	// in progress...
//	char* C_STR(string s) {
//		char* buffer = new char[32];
//		snprintf(buffer, 32, "%g", (*timers)[s] / 1000);
//		return buffer;
//	}

};

#endif

