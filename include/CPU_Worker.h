/*
 * CPU_Worker.h
 *
 *  Created on: Oct 31, 2012
 *      Author: rr
 */

#ifndef CPU_WORKER_H_
#define CPU_WORKER_H_

#include "core.h"
#include "pointerfreescene.h"
#include "hitpoints.h"
#include "List.h"
#include "cuda_utils.h"
#include "renderEngine.h"
#include "lookupAcc.h"
#include <xmmintrin.h>
#include "Profiler.h"
#include "luxrays/accelerators/qbvhaccel.h"
#include "config.h"
#include "Worker.h"
#include "atomic.h"

#include <hwloc.h>

class CPU_Worker;

static void result_set(const char *msg, int err, int supported) {
	const char *errmsg = strerror(errno);
	if (err)
		printf("%-40s: %sFAILED (%d, %s)\n", msg, supported ? "" : "X", errno,
				errmsg);
	else
		printf("%-40s: OK\n", msg);
}

static void result_get(const char *msg, hwloc_const_bitmap_t expected,
		hwloc_const_bitmap_t result, int err, int supported) {
	const char *errmsg = strerror(errno);
	if (err)
		printf("%-40s: %sFAILED (%d, %s)\n", msg, supported ? "" : "X", errno,
				errmsg);
	else if (!expected || hwloc_bitmap_isequal(expected, result))
		printf("%-40s: OK\n", msg);
	else {
		char *expected_s, *result_s;
		hwloc_bitmap_asprintf(&expected_s, expected);
		hwloc_bitmap_asprintf(&result_s, result);
		printf("%-40s: expected %s, got %s\n", msg, expected_s, result_s);
		free(expected_s);
		free(result_s);
	}
}

class CPU_Worker: public Worker {
public:

	/**
	 * in this worker used for raybuffer size
	 */
	uint WORK_SIZE;

	lookupAcc* lookupA;

	RayBuffer *rayBuffer;

	Seed* seedBuffer;

	HitPoint* HPsPositionInfo;
	//HitPointRadianceFlux* HPsIterationRadianceFlux;

	hwloc_obj_t numa;
	hwloc_bitmap_t cpuset;

	CPU_Worker(uint device, Engine* engine_) {

		size_t buffer_size = 1024 * 256;

		uint c = max(cfg->GetHitPointTotal(), cfg->photonsFirstIteration); //One seed per sample/hitpoint path and one seed per photon path

		seedBuffer = new Seed[c];

		for (uint i = 0; i < c; i++)
			seedBuffer[i] = mwc(i + device);
			//seedBuffer[i] = mwc(i);

		WORK_SIZE = buffer_size;

		deviceID = device;

		rayBuffer = new RayBuffer(buffer_size);

		if (cfg->CPU_AS == PARALLEL_HASHGRID)
			lookupA = new ParallelHashGridLookup(cfg->GetHitPointTotal(),
					cfg->rebuildHash);

		else if (cfg->CPU_AS == HASHGRID)
			lookupA = new HashGridLookup(cfg->GetHitPointTotal(),
					cfg->rebuildHash);

		else if (cfg->CPU_AS == KDTREE)
			lookupA = new KdTree();

		setScene(engine_->ss);

		engine = engine_;

		InitHitPoints();

		numa = hwloc_get_obj_by_type(cfg->topology, HWLOC_OBJ_SOCKET,
				deviceID - CPU0);

	}

	~CPU_Worker();

	void Start(bool buildHitPoints) {

		thread = new boost::thread(
				boost::bind(CPU_Worker::Entry, this, buildHitPoints));

	}

	inline uint GetWorkSize() {
		return WORK_SIZE;
	}

	BBox* GetHostBBox() {
		return lookupA->getBBox();
	}

	void resetRayBuffer(bool realloc = false) {

		rayBuffer->Reset();

		memset(rayBuffer->GetHitBuffer(), 0,
				sizeof(RayHit) * rayBuffer->GetSize());

		memset(rayBuffer->GetRayBuffer(), 0,
				sizeof(Ray) * rayBuffer->GetSize());

	}

//	HitPoint *GetHitPointInfo(const unsigned int index) {
//		return &(HPsPositionInfo)[index];
//
//	}

	HitPoint *GetHitPoint(const unsigned int index) {

		return &(HPsPositionInfo)[index];
	}

	void AdvanceEyePaths(RayBuffer *rayBuffer, EyePath *todoEyePaths,
			uint *eyePathIndexes);

	u_int64_t BuildPhotonMap(u_int64_t photontarget);

	void updateDeviceHitPointsInfo(bool toHost);
	void updateDeviceHitPointsFlux();
	void ResetDeviceHitPointsFlux();
	void ResetDeviceHitPointsInfo();
	void ProcessEyePaths();

	void InitHitPoints();

	void CommitIterationHitPoints(u_int64_t photonPerIteration);
	void MirrorHitPoints();

	float GetNonPAMaxRadius2();
	void SetNonPAInitialRadius2(float photonRadius2);

	void Intersect(RayBuffer *rayBuffer);
	void IntersectRay(const Ray *ray, RayHit *rayHit);
	void AccumulateFluxPPMPA(uint iteration, u_int64_t photonTraced);
	void AccumulateFluxPPM(uint iteration, u_int64_t photonTraced);
	void AccumulateFluxSPPM(uint iteration, u_int64_t photonTraced);
	void AccumulateFluxSPPMPA(uint iteration, u_int64_t photonTraced);

	void GetSampleBuffer();

	void UpdateBBox() {

		// Calculate hit points bounding box
		//std::cerr << "Building hit points bounding box: ";

		BBox hitPointsbbox = BBox();

		for (unsigned int i = 0; i < cfg->GetHitPointTotal(); ++i) {
			HitPoint *hp = GetHitPoint(i);

			if (hp->type == SURFACE)
				hitPointsbbox = Union(hitPointsbbox, hp->position);
		}

		SetBBox(hitPointsbbox);

//		printf("BBbox.min: %.10f %.10f %.10f BBbox.max: %.10f %.10f %.10f\n", hitPointsbbox.pMin.x,
//						hitPointsbbox.pMin.y, hitPointsbbox.pMin.z,
//						hitPointsbbox.pMax.x, hitPointsbbox.pMin.y,
//						hitPointsbbox.pMax.z);

	}

	void CopyLookupAccToDevice() {

	}

	void getDeviceHitpoints() {

	}

	void Bind_(int tid) {

#ifdef CHECK_AFFINITY
		uint core_logical_id = (tid % 6) + (deviceID - CPU0) * 6;
		uint socket_logical_id = (deviceID - CPU0);

		hwloc_cpuset_t result = hwloc_bitmap_alloc();
		hwloc_get_last_cpu_location(cfg->topology, result,
				HWLOC_CPUBIND_THREAD);

		hwloc_obj_t expected = hwloc_get_obj_by_type(cfg->topology,
				HWLOC_OBJ_CORE, core_logical_id);

		hwloc_obj_t obj_result = hwloc_get_obj_inside_cpuset_by_type(
				cfg->topology, result, HWLOC_OBJ_PU, 0);

#if defined(USE_SOCKET_AFFINITY) || defined(USE_SOCKET_CORE_AFFINITY)
		if (socket_logical_id
				!= obj_result->parent->parent->parent->parent->parent->parent->logical_index) {
			printf("Unexpected socket binding!\n");
		}
#endif

#ifdef USE_SOCKET_CORE_AFFINITY

		if (core_logical_id != obj_result->parent->logical_index) {
			printf("Unexpected core binding!\n");

		}
#endif
#endif

	}

	void Render(bool buildHitPoints);

	size_t getRaybufferSize() {
		return rayBuffer->GetSize();
	}

	void SetBBox(BBox hitPointsbbox) {
		lookupA->setBBox(hitPointsbbox);
	}

	size_t RaybufferAddRay(const Ray &ray) {
		return rayBuffer->AddRay(ray);
	}

	uint getRayBufferRayCount() {
		return rayBuffer->GetRayCount();
	}

	void AdvanceEyePaths(EyePath* todoEyePaths, uint* eyePathIndexes) {
		AdvanceEyePaths(rayBuffer, todoEyePaths, eyePathIndexes);
	}

	void advancePhotons(uint *donePhotonCount, uint *rayCount,
			uint *photonHitCount, const u_int64_t photonTarget);

	void IntersectRayBuffer() {
		Intersect(rayBuffer);
	}

	void UpdateQueryRangeLookupAcc(uint it) {

		double start = WallClockTime();

		float maxRad2 = GetCurrentMaxRadius2();

		lookupA->UpdateQueryRange(maxRad2, it, GetHitPoint(0));

		double elapsed = WallClockTime() - start;

		//fprintf(stderr, "Device %d: It %d Lookup update time: %.3f\n",
		//		getDeviceID(), it, elapsed);

	}

	void BuildLookupAcc() {

		double start = WallClockTime();
		float maxRad2 = GetCurrentMaxRadius2();

		lookupA->Build(maxRad2, GetHitPoint(0));

		double elapsed = WallClockTime() - start;

		//fprintf(stderr, "Device %d: Build Lookup time: %.3f\n", getDeviceID(),
		//		elapsed);
	}

	RayBuffer* GetRayBuffer() {
		return rayBuffer;
	}

	static void Entry(CPU_Worker *worker, bool buildHitPoints) {
		worker->Render(buildHitPoints);
	}

	static void photonTraceEntry(CPU_Worker *worker, uint *donePhotonCount,
			uint *rayCount, uint *photonHitCount,
			const u_int64_t photonTarget) {
		worker->advancePhotons(donePhotonCount, rayCount, photonHitCount,
				photonTarget);
	}

};

#endif /* CUDA_WORKER_H_ */
